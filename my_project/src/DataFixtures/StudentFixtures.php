<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Entity\Student;
use App\Entity\UserStudent;
use App\DataFixtures\ClassesFixtures;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


/*This class if used to create fake data for the data base : 
  create some students for each class of each school.*/
class StudentFixtures extends Fixture implements DependentFixtureInterface
{
    public function __construct(UserPasswordEncoderInterface $encoder){
        $this->encoder = $encoder;
    }
    
    public function load(ObjectManager $manager)
    {
       $faker = \Faker\Factory::create('fr_FR');
        for ($j=1; $j <=4 ; $j++) { 
            $classExample = array( "10th", "11th" ,"12th");
            foreach ($classExample as $key => $cl) {
                for ($i=1; $i <= mt_rand(5,10) ; $i++) {
                    $classesReference=$cl.$j.mt_rand(1,4);
                   $student = new Student();
                   $user = new User();
                   $userStudent = new UserStudent();
                   $gender = $faker->randomElement(array('male', 'female'));
                   $student->setLastname($faker->lastname())   
                           ->setFirstname($faker->firstname($gender))
                           ->setEmail($faker->email())
                
                 
                           ->addClass($this->getReference($classesReference));
                $user->setLastname($student->getLastname())
                     ->setGender($gender)
                     ->setFirstname($student->getFirstname())
                     ->setEmail($student->getEmail())
                     ->setSchool(($this->getReference($classesReference)->getSchool()))
                     ->setPassword($this->encoder->encodePassword($user, 'testtest'))
                     ->setRoles(array('ROLE_STUDENT'));
                     $userStudent->setUser($user);
                     $userStudent->setStudent($student);
                $user->setUserStudent($userStudent);
                  $manager->persist($student);
                  $manager->persist($user);
                  $manager->persist($userStudent);
               }
            }
            
        }
       

        $manager->flush();
    }


   public function getDependencies()
    {
        return array(
            ClassesFixtures::class,
        );
    }
}
