<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TeacherClassesRepository")
 */
class TeacherClasses
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Teacher", inversedBy="teacherClasses")
     * @ORM\JoinColumn(nullable=false)
     */
    private $teacher;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Subject")
     * @ORM\JoinColumn(nullable=true)
     */
    private $subject;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Classes", inversedBy="teacherClasses")
     * @ORM\JoinColumn(nullable=false)
     */
    private $classes;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SchoolSubject", inversedBy="teacherClasses")
     */
    private $school_subject;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTeacher(): ?teacher
    {
        return $this->teacher;
    }

    public function setTeacher(?teacher $teacher): self
    {
        $this->teacher = $teacher;

        return $this;
    }

    public function getSubject(): ?subject
    {
        return $this->subject;
    }

    public function setSubject(?subject $subject): self
    {
        $this->subject = $subject;

        return $this;
    }

    public function getClasses(): ?classes
    {
        return $this->classes;
    }

    public function setClasses(?classes $classes): self
    {
        $this->classes = $classes;

        return $this;
    }

    public function getSchoolSubject(): ?SchoolSubject
    {
        return $this->school_subject;
    }

    public function setSchoolSubject(?SchoolSubject $school_subject): self
    {
        $this->school_subject = $school_subject;

        return $this;
    }
}
