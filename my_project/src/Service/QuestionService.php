<?php // src/Service/MessageGenerator.php
namespace App\Service;

use App\Repository\ResponseRepository;
use App\Repository\QuestionStandardRepository;
use Doctrine\Common\Persistence\ObjectManager;

class QuestionService
{
    public function __construct(ObjectManager $manager, ResponseRepository $reporesponse,QuestionStandardRepository $repoqstandard){
        $this->manager= $manager;
        $this->reporesponse = $reporesponse;
        $this->repoqstandard =$repoqstandard;
    }
    public function isQuestionValid($question)  //this function will count the number of response in one question
    //if there are 0 responses this function will return false and if there are 1 or more response this function will return true.
    {
        if (($this->reporesponse->countByQuestion($question)>0)) {
            return true;
        }

        return false;
    }

    

    public function deleteQuestion($question){//Delete a question and all responses inside it.
        if($question)
        {
            if (($question->getQuestionStandard()) ==null) {
                $responses = $question->getResponses();
                if($responses !=null){
                    foreach ($responses as $key => $response) {
                        $this->manager->remove($response);
                        
                    
                }

                $this->manager->remove($question);
                $this->manager->flush();
            }
           
            
    
            }
           
        }
      
        }


   public function getQuestionRate($question){// return the rate of a question
       if($question->getType() == 'unique'){
           $result =$this->reporesponse->findOneMaxRatedResponseByQuestion($question);
           return $result['max_value'];
       }
       elseif ($question->getType() == 'multiple') {
           
            $responses = $question->getResponses();
            $result = 0;
            foreach ($responses as $key => $response) {
                if($response->getValue()>0)
                {
                    $result = $result + $response->getValue();
                }
            }
            return $result;
       }
       else return null;
   } 
        
}