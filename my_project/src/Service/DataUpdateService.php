<?php

namespace App\Service;

use App\Entity\TeacherPageMark;
use App\Repository\UserPageRepository;
use App\Repository\TeacherPageMarkRepository;
use Doctrine\Common\Persistence\ObjectManager;

class DataUpdateService 
{
    public function __construct(TeacherPageMarkRepository $teacherPageMarkRepository,
     UserPageRepository $userPageRepository, ObjectManager $manager){
        $this->teacherPageMarkRepository = $teacherPageMarkRepository;
        $this->userPageRepository = $userPageRepository;
        $this->manager            =$manager;
    }

    public function updateTeacherMark($page){
        $userPages=$this->userPageRepository->findByPage($page);
        $number_of_userPages = $this->userPageRepository->countByPage($page);
        $sum =0;
        foreach ($userPages as $key => $usrpgs) {
            $sum =$sum+$usrpgs->getMark();
        }
       
        $result =$sum/$number_of_userPages;
        $result = round($result,2);
        

        $teacherPageMark =$this->teacherPageMarkRepository->findOneByPage($page);
        if($teacherPageMark){
            $teacherPageMark->setMark($result)
                            ->setNumberOfResponses($number_of_userPages);
        }else{
            $teacherPageMark = new TeacherPageMark();
            $teacher = $page->getTeacher();
            $teacherPageMark->setTeacher($teacher)
                            ->setPage($page)
                            ->setMark($result)
                            ->setNumberOfResponses($number_of_userPages)
                            ;
        }

        $this->manager->persist($teacherPageMark);
        $this->manager->flush();
    }
}
