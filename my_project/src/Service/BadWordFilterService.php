<?php
namespace App\Service;


use Snipe\BanBuilder\CensorWords; //Library for a badword filter 
 

// This service is used before registering a comment from a student in the data base.
//If badwords have been found in the comment the comment must be ignored and not registered in the data base.
class BadWordFilterService
{

    private $censorWords;
    private $langs;

    public function __construct(){
        $this->censorWords = new CensorWords();
        $this->censorWords->addWhitelist(array("association")); // When a word like ASSociation is writed not consider the bad word part.
        $this->langs = array('en-base','cs','en-uk', 'de', 'fr', 'ru'); //Select the language of the badwords filter.
       
    }


    public function isBadWordsExist($text){
        $badwords = $this->censorWords->setDictionary($this->langs);

        $string = $this->censorWords->censorString($text);

        if(!empty($string['matched'])){
            return true;
        }
        else return false;

    }

    public function getBadWordsInfo($text){
        $badwords = $this->censorWords->setDictionary($this->langs);

        $string = $this->censorWords->censorString($text);
        return $string;
    }
}
