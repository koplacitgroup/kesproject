<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TeacherRepository")
 */
class Teacher
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=100, unique=true)
     */
    private $email;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TeacherClasses", mappedBy="teacher")
     */
    private $teacherClasses;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Subject")
     */
    private $subjects;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $teacher_code;

    public function __construct()
    {
        $this->teacherClasses = new ArrayCollection();
        $this->subjects = new ArrayCollection();
        $this->teacherSurveyMarks = new ArrayCollection();
        $this->groups = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return Collection|TeacherClasses[]
     */
    public function getTeacherClasses(): Collection
    {
        return $this->teacherClasses;
    }

    public function addTeacherClass(TeacherClasses $teacherClass): self
    {
        if (!$this->teacherClasses->contains($teacherClass)) {
            $this->teacherClasses[] = $teacherClass;
            $teacherClass->setTeacher($this);
        }

        return $this;
    }

    public function removeTeacherClass(TeacherClasses $teacherClass): self
    {
        if ($this->teacherClasses->contains($teacherClass)) {
            $this->teacherClasses->removeElement($teacherClass);
            // set the owning side to null (unless already changed)
            if ($teacherClass->getTeacher() === $this) {
                $teacherClass->setTeacher(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|subject[]
     */
    public function getSubjects(): Collection
    {
        return $this->subjects;
    }

    public function addSubject(subject $subject): self
    {
        if (!$this->subjects->contains($subject)) {
            $this->subjects[] = $subject;
        }

        return $this;
    }

    public function removeSubject(subject $subject): self
    {
        if ($this->subjects->contains($subject)) {
            $this->subjects->removeElement($subject);
        }

        return $this;
    }

   

    

    public function getTeacherCode(): ?string
    {
        return $this->teacher_code;
    }

    public function setTeacherCode(string $teacher_code): self
    {
        $this->teacher_code = $teacher_code;

        return $this;
    }
}
