<?php

namespace App\Repository;

use App\Entity\SurveyGroup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method SurveyGroup|null find($id, $lockMode = null, $lockVersion = null)
 * @method SurveyGroup|null findOneBy(array $criteria, array $orderBy = null)
 * @method SurveyGroup[]    findAll()
 * @method SurveyGroup[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SurveyGroupRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, SurveyGroup::class);
    }

    // /**
    //  * @return SurveyGroup[] Returns an array of SurveyGroup objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SurveyGroup
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
