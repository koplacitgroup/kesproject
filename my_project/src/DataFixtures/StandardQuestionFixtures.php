<?php

namespace App\DataFixtures;


use App\Entity\Question;
use App\Entity\Response;
use App\Entity\QuestionStandard;
use App\Entity\ResponseStandard;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

/*This class if used to create fake data for the data base : 
  create some question and response to create a standard survey automatically.*/
class StandardQuestionFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $standardResponses = [['Unsatifying',0],['Satisfying',1],['Good',2] ,['Very Good',2.5]];
        $standardQuestions =["How was the course ?","How was the quality of the document provided by the teacher ?"];
        foreach ($standardQuestions as $key => $standardQuestion) {
            $question = new Question();
            $question->setText($standardQuestion)
                     ->setType('unique')
                    ;
            $manager->persist($question);
            foreach ($standardResponses as $key => $standardResponse)
            {
                $response = new Response();
               
                $response->setText($standardResponse[0]);
                $response->setValue($standardResponse[1]);
                $response->setQuestion($question);
                $manager->persist($response);
                
            }
            $questionStandard= new QuestionStandard();
            $questionStandard->setQuestion($question);
            $manager->persist($questionStandard);
        }

        $manager->flush();
    }
}
