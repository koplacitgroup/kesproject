<?php

namespace App\Controller;

use App\Entity\Page;
use App\Service\PageService;
use App\Repository\PageRepository;
use App\Repository\SheetRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PageController extends AbstractController
{
    /**
     * @Route("/page", name="page")
     */
    public function index()
    {   //Basic test page.
        
        return $this->render('page/index.html.twig', [
            'controller_name' => 'PageController',
        ]);
    }


    /**
     * @Route("page/admin/delete/{survey_id}/{page_id}", name = "admin_delete_page")
     */
    public function deletePage($survey_id,$page_id, PageService $pageService,
    SheetRepository $reposurvey, PageRepository $repopage,ObjectManager $manager)
    {   //Delete a page.


        $page= $repopage->find($page_id);
        if($page)
        {
            $pageService->deletePage($page,$manager);

            return $this->redirectToRoute('admin_survey_page_home',['survey_id' => $survey_id]);

        }
        else
        {
            throw $this->createNotFoundException(
                'No page found for id '.$page_id
            );
        }
        
        }


        /**
         * @Route("page/show/{id}", name = "page_show")
         */
        public function showPage(Page $page){
            //Display a page (id, questions, teacher, subject).

            if($page)
            return $this->render('page/show.html.twig',['page' =>$page]);
            else{
                throw $this->createNotFoundException(
                    'No page found for id '.$page->getId()
                );
            }
        }

        
}

