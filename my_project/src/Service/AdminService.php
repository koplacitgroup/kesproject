<?php // src/Service/MessageGenerator.php
namespace App\Service;

use App\Repository\SheetRepository;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;

class AdminService
{
    public function __construct(SheetRepository $reposurvey){
        $this->reposurvey = $reposurvey;
    }


    public function generateCSVFile($survey)
    {
        
 
    //Create a new Filesystem objet to manage file in Symfony
    $filesystem = new Filesystem();

    try {
        //Create a temporary file on the server.
        $temp_file = $filesystem->tempnam(sys_get_temp_dir(), 'prefix_');
    } catch (IOExceptionInterface $exception) {//Catch some exeptions in case of errors.
        echo "An error occurred while creating your directory at ".$exception->getPath();
    }
        
   


    //Collect the anonymousId list from the DataBase.
    $anonymousIds = $survey->getAnonymousIds();

    //Create the name of the future downloaded file.
    $fileName = 'export.csv';
    


    //Open the file in writing mode.
    $handler=fopen($temp_file,'w');

    //Writing the columns headers of the file.
    fprintf($handler, chr(0xEF).chr(0xBB).chr(0xBF));
    fputcsv($handler, array('special_id', 'password', 'valid'),';');

 
    //Output each row of the data.
    foreach ($anonymousIds as $key => $anId)
    {
      if(!$anId->getValid()){
        $valid = '0';
      }
      else{
          $valid= $anId->getValid();
              }
    fputcsv($handler, array($anId->getId(),$anId->getPassword(),$valid),';');
    }

    //Close the file 
    fclose($handler);

    //return the file to download and rename it with the "$fileName".
        return array('temp_file'=>$temp_file,'file_name' => $fileName);
    }
}
  
