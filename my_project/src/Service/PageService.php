<?php // src/Service/MessageGenerator.php
namespace App\Service;

use App\Service\QuestionService;
use App\Repository\PageRepository;
use App\Repository\QuestionRepository;
use Doctrine\Common\Persistence\ObjectManager;

class PageService
{
    public function __construct(QuestionService $questionService,QuestionRepository $repoquestion,PageRepository $pageRepository){
        $this->questionService = $questionService;
    
        $this->repoquestion = $repoquestion;

        $this->pageRepository = $pageRepository;
        
    }


    public function isPageValid($page)
    {
        if (($this->repoquestion->countByPage($page)>0)) {
            $questions = $page->getQuestions();
            foreach ($questions as $key => $question) {
                if(!($this->questionService->isQuestionValid($question)))
                return false;
            }
       
            return true;
        }

        return false;
    }

    public function countQuestion($page){
        return ($page->getQuestions())->length();

    }

    public function deletePage($page,$manager){
        $questions= $page->getQuestions();
        if(!empty($questions)){
            foreach ($questions as $key => $question) {
            if($question->getId()!=null){
            
               
                $this->questionService->deleteQuestion($question,$manager);
                
            }
            }
            $this->pageRepository->removeAllQuestion($page);
        }
        
        $manager->remove($page);
        $manager->flush();
       
    }

    

}