<?php

namespace App\Repository;

use App\Entity\AnonymousIdPageMark;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method AnonymousIdPageMark|null find($id, $lockMode = null, $lockVersion = null)
 * @method AnonymousIdPageMark|null findOneBy(array $criteria, array $orderBy = null)
 * @method AnonymousIdPageMark[]    findAll()
 * @method AnonymousIdPageMark[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AnonymousIdPageMarkRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, AnonymousIdPageMark::class);
    }

    // /**
    //  * @return AnonymousIdPageMark[] Returns an array of AnonymousIdPageMark objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AnonymousIdPageMark
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
