<?php

namespace App\Controller;

use App\Entity\Page;
use App\Entity\Sheet;
use App\Entity\School;
use App\Form\PageType;
use App\Entity\Classes;
use App\Entity\Subject;
use App\Entity\Teacher;
use App\Entity\Question;
use App\Entity\Response;
use App\Form\SurveyType;
use App\Form\CreationType;
use App\Entity\SurveyGroup;
use App\Service\AdminService;
use App\Service\SurveyService;
use App\Entity\SurveyInProgress;
use App\Repository\PageRepository;
use App\Repository\SheetRepository;
use App\Repository\SchoolRepository;
use App\Repository\ClassesRepository;
use App\Repository\SubjectRepository;
use App\Repository\TeacherRepository;
use App\Service\SelectSubjectService;
use App\Repository\ResponseRepository;
use App\Repository\AdminSchoolRepository;
use App\Repository\TeacherClassesRepository;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\QuestionStandardRepository;
use App\Repository\SurveyInProgressRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminController extends AbstractController
{
    /**
    * @Route("/admin", name="admin")
    */
    public function index()
    {
        return $this->render('admin/index.html.twig');
    }

    /**
    * @Route("/admincz", name="admincz")
    */
    public function indexCZ()
    {
        return $this->render('admin/cz/indexcz.html.twig');
    }
    
    // ADMIN MANAGE SURVEYS

    /**
    * @Route("/admin/survey/home", name="admin_survey_home")
    */
    public function getSurveyHomePage(SheetRepository $repoSheet, SurveyService $surveyService,ObjectManager $manager)
    {   //Display a main page with the lisgt of all surveys from one school
        $user= $this->getUser();
        $table=array();
        $school = $user->getSchool();
        $surveys = $repoSheet->findBySchool($school);

        return $this->render('admin/survey.home.html.twig',[
            'school' => $school,'surveys' => $surveys
        ]);
    }
    
    /**
    * @Route("admin/survey/refresh", name="admin_survey_refresh")
    */
    public function refreshSurvey(SheetRepository $reposurvey,SurveyService $serviceService, ObjectManager $manager ){
        //Update the state of all suveys.
            $school = $this->getUser()->getSchool();
            $surveys = $reposurvey->findBySchool($school);
            foreach ($surveys as $key => $srv) {
                $survey=$reposubject->find($srv);
                if($surveyService->isSurveyValid($survey))
                {
                    $survey->setSendable(true);
                    $manager->persist($survey);
                }
                $manager->flush();
            }
    }

    /**
    * @Route("/admin/survey/create/new", name="admin_survey_create")
    */
    public function createSurvey(Request $request , ClassesRepository $clsrepo, ObjectManager $manager){

        //Create a new survey
        $survey = new Sheet();
        $school = $this->getUser()->getSchool();
       
        $form = $this->createForm(SurveyType::class,$survey);
        $form->handleRequest($request);


        if (  $form->isSubmitted() && $form->isValid()  )
    
        {
            $classes =$clsrepo->findOneById($request->request->get('classesId'));
            if($request->request->get('auto')=='yes'){
                return $this->redirectToRoute('admin_survey_create_standard',['id'=> $classes->getId(), 'title' =>$survey->getTitle()]);
            }
        
            
            $survey->setClasses($classes);
            $survey->setCreatedAt(new \DateTime());
            $survey->setSendable(true);
            $survey->setSended(false);
            $manager->persist($survey);
            $manager->flush();
    
            return $this->redirectToRoute('admin_survey_page_home',['survey_id' =>$survey->getId()]);
        }
        else
        return $this->render('admin/new.survey.html.twig',[
            'school'=> $school,'form' => $form->createView()
        ]);
    }

    /**
    * @Route("/admin/survey/{survey_id}", name="admin_survey_page_home")
    */
    public function createSurveyView($survey_id, SheetRepository $reposurvey, SurveyService $surveyService){
        // Display the survey and the list of pages edited by the admin
        $survey = $reposurvey->findOneById($survey_id);

             return $this->render('admin/survey.page.home.html.twig',['survey'=> $survey]);
    }





    /**
    * @Route("admin/survey/{survey_id}/create/page/new/select_teacher", name="admin_survey_page_new_select_teacher")
    */
    public function createPageSelectTeacher($survey_id, SheetRepository $reposurvey,Request $request)
    {       //Select the teacher concerned by this page from one survey
            $survey=$reposurvey->findOneById($survey_id);
            $classroom = $survey->getClasses();
            $teachersClasses = $classroom->getTeacherClasses();
            $teachers = array();
            foreach ($teachersClasses as $key => $tcl)
            {
                $teachers[] = $tcl->getTeacher();
            } 
            if( isset( $request->request ) && $request->request->get('teacher') != null )
            {
                $teacher_id = $request->request->get('teacher');

                return $this->redirectToRoute('admin_survey_page_new_select_subject', ['survey_id' => $survey_id, 'teacher_id' => $teacher_id]);
            }
            return $this->render('admin/survey.page.create.select.teacher.html.twig',[
                'survey'=>$survey, 'teachers'=>$teachers
            ]);
    }





    /**
    * @Route("admin/survey/{survey_id}/create/page/new/select_subject_for_teacher/{teacher_id}", name="admin_survey_page_new_select_subject")
    */
    public function createPageSelectSubject($survey_id,$teacher_id, Request $request, TeacherClassesRepository $repoteacherclasses,
    SheetRepository $reposurvey, TeacherRepository $repoteacher)
    {   // Select a subject for 1 page from 1 survey

        $survey =$reposurvey->findOneById($survey_id);
        $teacher = $repoteacher->findOneById($teacher_id);
        
        $classroom = $survey->getClasses();
        $teacherClasses = $repoteacherclasses->findByTeacherAndClasses($teacher,$classroom);
        $subjects = array();

        foreach ($teacherClasses as $key => $tcl)
        {
            $subjects[]= $tcl->getSubject();
        }

        // If the form to select subject is submitted redirect to the route to create the questions and response of the pages.
        if(isset($request->request)  && $request->request->get('subject') != null)
        {
            //Get the subject entity object form the database.
            $reposubject = $this->getDoctrine()->getRepository(Subject::class);
            $subject = $reposubject->findOneById($request->request->get('subject'));
            //Find the page the user want to complete the creation form the data base 
            $manager= $this->getDoctrine()->getManager();
            $repopage = $this->getDoctrine()->getRepository(Page::class);
            $page = $repopage->findBy([
                'sheet' => $survey,
                'teacher' => $teacher,
                'subject' => $subject
            ]);

            if(!$page)
            {
                $page = new Page();
                $page->setSheet($survey);
                $page->setTeacher($teacher);
                $page->setSubject($subject);
                $manager->persist($page);
                $survey->setUpdatedAt( new \DateTime());
                $manager->persist($survey);
                $manager->flush();

            return $this->redirectToRoute('admin_survey_page_home', ['survey_id' => $survey_id]);  
            }
            else
            {
                return $this->redirectToRoute('admin_survey_page_new_select_subject', ['survey_id' => $survey_id, 'teacher_id' => $teacher_id]);
            }
        }
        else
            return $this->render('admin/survey.page.create.select.subject.html.twig',[
                'survey'=>$survey, 'teacher'=>$teacher, 'subjects' => $subjects
            ]);
    }




    /**
    *@Route("/admin/survey/send/home", name="survey_send")
    */
    public function sendSurvey(Request $request, SurveyService $surveyService, SheetRepository $reposurvey, ObjectManager $manager)
    {    // When a survey is reasy to send send the survey to the classes that is concerned


       if(!empty( $surveyId=$request->request->get('survey_id')))
       {    
           $survey = $reposurvey->find($surveyId);
           if($survey->getSendable() && !( $survey->getSended() )  ){
            $surveyInProgress = new SurveyInProgress();
            $surveyInProgress->setSurvey($survey);
            $surveyInProgress->setSendedAt(new \DateTime());
            $survey->setSended(true);
            $manager->persist($surveyInProgress);
            $manager->persist($survey);
            $manager->flush();
            $classroom =($survey->getClasses())->getName();
            
            $anonymousIds = $surveyService->generateAnonymousId($survey,$manager);

            return $this->render('admin/survey.send.html.twig',['classroom' =>$classroom, 'anonymousIds'=>$anonymousIds,'survey'=>$survey]);
           }
           else{
               return $this->render('admin/survey.send.html.twig', ['classroom'=>null,'anonymousIds'=>null]);
           }

       }
    }





    /**
    * * @Route("admin/create/standard_survey/{id}/{title}", name="admin_survey_create_standard")
    */
    public function createStandardSurvey(Classes $classes, $title,SurveyService $surveyService, TeacherClassesRepository $repoteacherclasses, QuestionStandardRepository $repoquestionstandard, ObjectManager $manager)
    {
        // Create a survey automatically for one class
        $surveyService->createStandardSurvey($classes,$title, $repoquestionstandard,$manager);

        return $this->redirectToRoute('admin_survey_home');
    }






    /**
    * @Route("admin/download/anonymous/id/list/survey/{id}", name="admin_download_anonymousIdList")
    */
    public function downloadAnonymousIdList(Sheet $survey, AdminService $adminService){
        $file = $adminService->generateCSVFile($survey);
        return $this->file($file['temp_file'], $file['file_name']);
    }





    /**
    *@Route("/admin/survey/page/questions/create", name="admin_survey_page_questions_create")
    */
    public function surveyPageQuestionsCreate(Request $request, PageRepository $pageRepository)
    {

        //After the survey was created genereate anonymousId for each student od the classroom.
        //The students will use these Ids with password to have a unique access to the survey and fill it up. 
        $variable =intval($request->request->get('number_of_questions'));
        $pageId=$request->request->get('page_id');
        
        $page= $pageRepository->find($pageId);
        if($page){
            return $this->render('admin/questioncreating.html.twig',['page'=>$page,'variable'=>$variable]);
        } else {
            throw $this->createNotFoundException(
                'No page found for id '.$page->getId()
            );
        }
       
    }





    /**
    * @Route("admin/modification", name="admin_modification")
    */
    public function modification(SheetRepository $repoSheet)
    {
        //Link to modify a survey
        $user = $this->getUser();
        $school = $user->getSchool();
        $surveys = $repoSheet->findBySchool($school);

        return $this->render('admin/modify.html.twig', ['surveys' => $surveys]);
    }






    /**
    * @Route("/admin/deletions", name="admin_deletion") 
    */
    public function index3(SheetRepository $repoSheet)
    {//Link to delete one survey
        $user= $this->getUser();

        $school = $user->getSchool();
        $surveys = $repoSheet->findBySchool($school);
       
        return $this->render('admin/pagedelete.html.twig',[
            'school' => $school,'surveys' => $surveys
        ]);
    }







    /**
    * @Route("/admin/howmuch/{id}", name="how_much") 
    */
    public function howmuchquestions(Page $page)
    { //Select the number of question the admin wants to create to the page form one survey during the creating of one page.
        return $this->render('admin/how.much.html.twig',['page_id' =>$page->getId()]);
    }

    /**
    * @Route("/admin/adds", name="admin_create_survey_manualy") 
    */
    public function addingSurvey(Request $request, ObjectManager $manager, PageRepository $pageRepository)
    {//Display a form to create a custom page to one survey.
        

        //Condition to see if the form is submitted.
        if($page =$pageRepository->find($request->request->get('page_id')))
        {
            //Collect Data from form.
            $howmanyques = $request->request->get('how_many_questions');//Number of question submitted
            $commentsection = $request->request->get('comment_section');//If the user wants to this survey a section to comment. 


                    //Put the questions and responses data in a array().
            $questionlist = array();
            for ($i=1; $i <= $howmanyques; $i++)
            { 
                //Condition to verify if the questions inputs are no empty (more than 2 characters).
                if(strlen($request->request->get('question'.$i.'_text'))>=2){
                    $questionList['question'.$i]['question'.$i.'_text'] = $request->request->get('question'.$i.'_text');
                    $questionList['question'.$i]['question'.$i.'_type'] = $request->request->get('question'.$i.'_type');
                    $questionList['question'.$i]['question'.$i.'_aswers']=array();
                    for($y=1; $y <= 5; $y++)
                    {   if($request->request->get('question'.$i.'_answ'.$y.'_text') !=null){
                        $questionList['question'.$i]['question'.$i.'_aswers'][] =['text'=> $request->request->get('question'.$i.'_answ'.$y.'_text'),
                    'value' => $request->request->get('question'.$i.'_answ'.$y.'_value')];
                        
                    }
                }
               
                  
                }   
            }

        
            //For each question put the data of questions and responses in the database
           for ($i=1; $i <= $howmanyques; $i++)
            {
                
                $question = new Question(); //Create a new question object
                $question->setText($questionList['question'.$i]['question'.$i.'_text']) /*put the text of the question 
                from the form in the question Entity Object*/

                         ->setType($questionList['question'.$i]['question'.$i.'_type']) /*put the which type of question it
                          is from the data of the form submitted*/
                         ;
                         
               //Keep in memory the question before save it in the data base.
                $manager->persist($question);
                
                //Circle to get for each question the responses added by the user in the form.
                foreach ($questionList['question'.$i]['question'.$i.'_aswers'] as $key => $value )
                {
                    $response = new Response();// Create a new Response Entity Object.
                    $response->setText($value['text']) //Add the text of the response.
                            ->setQuestion($question)  //Link the question and the response.
                             ->setValue($value['value']) //Add the number of point of the response.
                             ;

                             //Keep the response entity object in mememory to save it later in the data base (with a manager->flush())
                    $manager->persist($response);
                }

                //Link the question with the page.
                $page->addQuestion($question);
            }
           
            //Keep the page created in memory before save it in the database.
            $manager->persist($page);

           
            //Save all the page, questions and response created in the database.
            $manager->flush();

            //Display the page the user has just created.
        return $this->render('sheet/survey.show.one.html.twig',['survey'=>$page->getSheet()]);
        }
         
        //Display the form to create a new page with questions.
        return $this->render('admin/survey.creation.html.twig');
    }

    /**
    * @Route("/admin/modification/finish", name="modify_finish") 
    */
    public function finishingmodification(Request $request, SheetRepository $repoSheet, SurveyService $surveyService,ObjectManager $manager)
    {// Display the surveys of the School when  a modification is finished.
       
        $table = $request->request->get('surv_id');
        $surveys =array();

        foreach ($table as $key => $value)
        {
            $surveys[] = $repoSheet->find($value);
        }
        
        $user = $this->getUser();
        $school = $user->getSchool();
        $table = array();

        return $this->render('admin/modify.finish.html.twig',[
            'school' => $school,'surveys' => $surveys
        ]);
    }



    /**
     * @Route("admin/create_survey/select/{mode}", name="create_survey_select")
     */
    public function selectCreationSurveyMode(Request $request,$mode=null,  ClassesRepository $classesRepository, SurveyService $surveyService, ObjectManager $manager){
        if($mode=='all_school'){

if($request->get('all_school_submitted'))
            {
                if($request->get('custom_all_school') =="1"){
                    return  $this->redirectToRoute('survey_how_much_questions_all_school');
                }
                elseif($request->get('custom_all_school') =="0"){
                   
                   $school = ($this->getUser())->getSchool();
                   $year = (new \DateTime())->format('Y');
                   $surveyService->createSurveyForAllSchool($school, $year, true, null);
                  return $this->render('admin/survey.creation.html.twig');
                }
            }


            return $this->render('admin/creation.survey.mode.html.twig', ['mode'=>'all_school', 'form'=>null]);
        }elseif ($mode =='select_classes') {
            $school = ($this->getUser())->getSchool();
            $year = (new \DateTime())->format('Y');
        
            $classesList = $classesRepository->findBy(['year' => $year,
            'school' => $school]);
            
              $form = $this->createFormBuilder()
                
                ->add('classes', EntityType::class,[
                    'class' => Classes::class,
                    'choices' => $classesList,
                    'choice_label' => 'name',
                    'expanded'=> false,
                    'multiple' => true
                ])
               ->add('custom_survey', ChoiceType::class,
               
    
    [
        'label'=>'Do you want to edit your own question to these surveys ?',
        'choices' => [
            'Yes'=>true,
            'No'=>false],

        'choice_label'=> function ($choice, $key, $value) {
            if (true === $choice) {
                return 'Yes, I want to create personalised questions and response for the survey.';
            }
            else
            {
               return 'No I want to create survey automatically with the standard questions and responses.'; 
            }
            return strtoupper($key);

            // or if you want to translate some key
            //return 'form.choice.'.$key;
        },
       // 'expanded' => true,
        //'multiple' => false
    
    
    ])
                ->add('send', SubmitType::class)
                ->getForm();

                $form->handleRequest($request);
                if($form->isSubmitted() && $form->isValid()){
                $data = $form->getData();
                $classes = $data['classes'];
                $custom_survey = $data['custom_survey'];
               
                if($custom_survey){
                    $surveyGroup = new SurveyGroup();
                    foreach ($classes as $key => $cls) {
                        $survey = new Sheet();
                        $survey->setClasses($cls);
                        $survey->setcreatedAt(new \DateTime());
                        $survey->setTitle('Survey of '.$cls->getName().' '.$cls->getYear());
                        $survey->setSendable(false);
                        $survey->setSended(true);
                        $survey->addSurveyGroup($surveyGroup);

                        $manager->persist($survey);
                      


                    }
                    $manager->persist($surveyGroup);
                    $manager->flush();
               return $this->redirectToRoute('survey_how_much_questions', ['id'=>$surveyGroup->getId()]);
                }
                else{
                    $surveyService->createSurveyForClasses($classes, true, null );
                }

                }

            return $this->render('admin/creation.survey.mode.html.twig', ['mode'=>'select_classes', 'form'=>$form->createView()]);
        }
        elseif($mode==null){
            return $this->render('admin/creation.survey.mode.html.twig', ['mode'=>null, 'form'=>null]);
        }
    }


    /**
     * @Route("/admin/creation/survey_question/how_much/{id}", name="survey_how_much_questions")
     */
    public function surveyHowMuchQuestions(SurveyGroup $surveyGroup,Request $request){
        if($request->get('number_of_questions')){
            $number = $request->get('number_of_questions');
            
            return $this->redirectToRoute('admin_survey_create_questions', ['id'=>$surveyGroup->getId(), 'how_much'=>$number]);
        }
        return $this->render('admin/creation.survey.number.question.html.twig',['surveyGroupId'=>$surveyGroup->getId()]);

    }


    /**
     * @Route("/admin/creation/survey_question/surveyGroup/{id}/{how_much}", name="admin_survey_create_questions")
     */
    public function surveyCreationQuestionCustom(SurveyGroup $surveyGroup,Request $request, $how_much, SurveyService $surveyService, ObjectManager $manager ){
if($request->get('survey_group_id')){


        
      //Collect Data from form.
      $howmanyques = $request->request->get('how_many_questions');//Number of question submitted
     


              //Put the questions and responses data in a array().
      $questionlist = array();
     
      for ($i=1; $i <= $howmanyques; $i++)
      { 
          //Condition to verify if the questions inputs are no empty (more than 2 characters).
          if(strlen($request->request->get('question'.$i.'_text'))>=2){
              $questionList['question'.$i]['question'.$i.'_text'] = $request->request->get('question'.$i.'_text');
              $questionList['question'.$i]['question'.$i.'_type'] = $request->request->get('question'.$i.'_type');
              $questionList['question'.$i]['question'.$i.'_aswers']=array();
              for($y=1; $y <= 5; $y++)
              {   if($request->request->get('question'.$i.'_answ'.$y.'_text') !=null){
                  $questionList['question'.$i]['question'.$i.'_aswers'][] =['text'=> $request->request->get('question'.$i.'_answ'.$y.'_text'),
              'value' => $request->request->get('question'.$i.'_answ'.$y.'_value')];
                  
              }
          }
         
            
          }   
          
      }

  
      //For each question put the data of questions and responses in the database
     for ($i=1; $i <= $howmanyques; $i++)
      {
          
          $question = new Question(); //Create a new question object
          $question->setText($questionList['question'.$i]['question'.$i.'_text']) /*put the text of the question 
          from the form in the question Entity Object*/

                   ->setType($questionList['question'.$i]['question'.$i.'_type']) /*put the which type of question it
                    is from the data of the form submitted*/
                   ;
                   
         //Keep in memory the question before save it in the data base.
        
          $manager->persist($question);
            $surveyGroup->addQuestion($question);
             $manager->persist($surveyGroup);
          
          
          //Circle to get for each question the responses added by the user in the form.
          foreach ($questionList['question'.$i]['question'.$i.'_aswers'] as $key => $value )
          {
              $response = new Response();// Create a new Response Entity Object.
              $response->setText($value['text']) //Add the text of the response.
                      ->setQuestion($question)  //Link the question and the response.
                       ->setValue($value['value']) //Add the number of point of the response.
                       ;

                       //Keep the response entity object in mememory to save it later in the data base (with a manager->flush())
              $manager->persist($response);
          }

          //Link the question with the page.
         // $page->addQuestion($question);
      }
     
      //Keep the page created in memory before save it in the database.
     // $manager->persist($page);

     
      //Save all the page, questions and response created in the database.
      $manager->flush();


      $year = (new \DateTime())->format('Y');
          $surveyService->createSurveyFromSurveyGroup($surveyGroup);
      

      return $this->render('admin/survey.creation.html.twig');
}
        return $this->render('admin/creation.survey.question.html.twig',['surveyGroupId'=>$surveyGroup->getId(),'number_of_questions'=>$how_much]);
    }





    /**
     * @Route("admin/survey/howmuch/all_school",name="survey_how_much_questions_all_school")
     */
    public function createSurveyAllSchool( Request $request){
        if($request->get('number_of_questions')){
            $number_of_questions = $request->get('number_of_questions');
            return $this->redirectToRoute('admin_all_school_create_questions',['number_of_questions'=>$number_of_questions]);
        }
        return $this->render('admin/creation.survey.all.school.number.question.html.twig');
    }


    /**
     * @Route("admin/survey/create/all_school/questions/{number_of_questions}",name="admin_all_school_create_questions")
     */
    public function createSurveyAllSchoolQuestions( Request $request, $number_of_questions, ObjectManager $manager, SurveyService $surveyService){
        if($request->get('all_school_questions_submitted')){
            $school = ($this->getUser())->getSchool();
            
               
      //Collect Data from form.
      $howmanyques = $request->request->get('how_many_questions');//Number of question submitted
     


      //Put the questions and responses data in a array().
$questionlist = array();
$questionObjectList = array();
for ($i=1; $i <= $howmanyques; $i++)
{ 
  //Condition to verify if the questions inputs are no empty (more than 2 characters).
  if(strlen($request->request->get('question'.$i.'_text'))>=2){
      $questionList['question'.$i]['question'.$i.'_text'] = $request->request->get('question'.$i.'_text');
      $questionList['question'.$i]['question'.$i.'_type'] = $request->request->get('question'.$i.'_type');
      $questionList['question'.$i]['question'.$i.'_aswers']=array();
      for($y=1; $y <= 5; $y++)
      {   if($request->request->get('question'.$i.'_answ'.$y.'_text') !=null){
          $questionList['question'.$i]['question'.$i.'_aswers'][] =['text'=> $request->request->get('question'.$i.'_answ'.$y.'_text'),
      'value' => $request->request->get('question'.$i.'_answ'.$y.'_value')];
          
      }
  }
 
    
  }   

}


//For each question put the data of questions and responses in the database
for ($i=1; $i <= $howmanyques; $i++)
{
  
  $question = new Question(); //Create a new question object
  $question->setText($questionList['question'.$i]['question'.$i.'_text']) /*put the text of the question 
  from the form in the question Entity Object*/

           ->setType($questionList['question'.$i]['question'.$i.'_type']) /*put the which type of question it
            is from the data of the form submitted*/
           ;
           
 //Keep in memory the question before save it in the data base.

 $questionObjectList[]=$question;
 dump($questionObjectList);
  $manager->persist($question);
  
  
  
  //Circle to get for each question the responses added by the user in the form.
  foreach ($questionList['question'.$i]['question'.$i.'_aswers'] as $key => $value )
  {
      $response = new Response();// Create a new Response Entity Object.
      $response->setText($value['text']) //Add the text of the response.
              ->setQuestion($question)  //Link the question and the response.
               ->setValue($value['value']) //Add the number of point of the response.
               ;

               //Keep the response entity object in mememory to save it later in the data base (with a manager->flush())
      $manager->persist($response);
  }

    //Link the question with the page.
    // $page->addQuestion($question);
    }

    //Keep the page created in memory before save it in the database.
    // $manager->persist($page);


    //Save all the page, questions and response created in the database.
    $manager->flush();


        $year = (new \DateTime())->format('Y');
        $school =($this->getUser())->getSchool();
        $surveyService->createSurveyForAllSchool($school,$year,false,$questionObjectList);



            return $this->render('admin/survey.creation.html.twig');
        }
        return $this->render('admin/creation.survey.all.school.question.html.twig', ['number_of_questions'=> $number_of_questions]);
    }
}