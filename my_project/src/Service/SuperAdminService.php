<?php

namespace App\Service;

use App\Entity\User;
use App\Entity\Classes;
use App\Entity\Teacher;
use App\Entity\UserTeacher;
use App\Entity\SchoolSubject;
use App\Repository\UserRepository;
use App\Repository\ClassesRepository;
use App\Repository\TeacherRepository;
use App\Repository\SchoolSubjectRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;



class SuperAdminService

{
    public function __construct(ObjectManager $manager, 
    ClassesRepository $classesRepository, TeacherRepository $teacherRepository, UserRepository $userRepository,
    UserPasswordEncoderInterface $encoder, SchoolSubjectRepository $schoolSubjectRepository){
     
        $this->manager                     = $manager;
        $this->classesRepository           = $classesRepository;
        $this->teacherRepository           = $teacherRepository;
        $this->userRepository              = $userRepository;
        $this->encoder                     = $encoder;
        $this->schoolSubjectRepository     = $schoolSubjectRepository;
    }


    public function insertClasses($input_name, $school){
        if(isset($_FILES[$input_name])){
           //This function is to insert Classes in the database by using 
                
                $fileName = $_FILES[$input_name]["tmp_name"];
    
                if($_FILES[$input_name]["size"] > 0)
                {
                    $file =fopen($fileName,"r");
                    $column=fgetcsv($file,10000,";");
                    
                    while(($column = fgetcsv($file,10000,";"))!==FALSE)
                    {   
                        if( !$this->classesRepository->findOneBy(['name'=>$column[0] ,'year'=>$column[1], 'school'=>$school]) ){
                            $classes = new Classes();
                            $classes->setName($column[0]);
                            $classes->setYear($column[1]);
                            $classes->setSchool($school);
                            $this->manager->persist($classes);
                        }
                        
                        
    
                    }
                    $this->manager->flush();
                }
            }
    }


    public function insertTeachers($input_name, $school){
            if(isset($_FILES[$input_name])){
               
                    
                    $fileName = $_FILES[$input_name]["tmp_name"];
        
                    if($_FILES[$input_name]["size"] > 0)
                    {
                        $file =fopen($fileName,"r");
                        $column=fgetcsv($file,10000,";");
                        
                        while(($column = fgetcsv($file,10000,";"))!==FALSE)
                        {   
                            if( !$this->teacherRepository->findOneByEmail(strtolower($column[3])) ){
                                $teacher = new Teacher();
                                $userTeacher = new UserTeacher();
                                $teacher->setLastname($column[0])
                                        ->setFirstname($column[1])
                                        ->setEmail(strtolower($column[3]))
                                        ->setTeacherCode($column[4])
                            ;

                                $user = new User();
                                $user->setFirstname($column[0])
                                    ->setLastname($column[1])
                                    ->setGender($column[2])
                                    ->setEmail(strtolower($column[3]))
                                    ->setSchool($school)
                                    ->setRoles(array('ROLE_TEACHER'))
                   ;
                        $password = '12345678';
                        $hash = $this->encoder->encodePassword($user, $password);
                        $user->setPassword($hash);

                        $userTeacher->setUser($user)
                                    ->setTeacher($teacher)
                                    ;

                        $user->setUserTeacher($userTeacher)
                            ;


                        $this->manager->persist($user);
                        $this->manager->persist($teacher);
                        $this->manager->persist($userTeacher);        
                           
                            }
                            
                            
        
                        }
                        $this->manager->flush();
                    }
                }       
    }

    public function insertSchoolSubjectsCsv($input_name, $school,$filter){
        $dataCollected= array();
        $nubrtab = array("1","2","3","4","5","6","7","8","9","0"," ");
        if(isset($_FILES[$input_name])){
               
                    
            $fileName = $_FILES[$input_name]["tmp_name"];

            if($_FILES[$input_name]["size"] > 0)
            {
                $file =fopen($fileName,"r");
                $column=fgetcsv($file,10000,";");
                
                
                while(($column = fgetcsv($file,10000,";"))!==FALSE)
                {   $sbjname  = str_replace(array(" "),"",$column[0]);   
                    $sbjcode = str_replace(array(" "),"",$column[1]);
                  

                        if($filter==true){
                            
                            if(preg_match('#[0-9]$#',$sbjcode)){
                                $sbjcode = str_replace($nubrtab,'',$column[1]);
                            }
                            
                        }
                        if($dataCollected!=array()){
                            
                            if(!$this->isTableContains($dataCollected,$sbjcode)){
                            
                                $dataCollected[]=[0=>$sbjname,1=>$sbjcode];
                            }
                        }
                        else{
                            $dataCollected[]=[0=>$sbjname,1=>$sbjcode];
                        }
                            
                }
                foreach ($dataCollected as $key => $dc) {
                    if(! $this->schoolSubjectRepository->findOneBy(['subject_code'=>$dc[1], 'school'=>$school])){ 
                        
                        $schoolSubject = new SchoolSubject(); 
                        $schoolSubject->setSchool($school)
                                    ->setSubjectName($dc[0])
                                    ->setSubjectCode($dc[1])
                                    ;
                        $this->manager->persist($schoolSubject);
                        
                    }
                }
                dump($dataCollected);
                       
                
                }
                $this->manager->flush();
            }
                
        
        }
    
    
    
    
        function isTableContains($table,$string){
            foreach ($table as $key => $tbl) {
                
                 if (strtoupper($tbl[1]) == strtoupper($string)){
                return true;
                }
            }
            return false; 
        }
    
        function no_accentNEW($text) {
            $utf8 = array(
                '/[áàâãªäåæ]/u' => 'a',
                '/[ÁÀÂÃÄÅÆ]/u' => 'A',
                '/[éèêëě]/u' => 'e',
                '/[ÉÈÊË]/u' => 'E',
                '/[ÍÌÎÏÍ]/u' => 'I',
                '/[íìîï]/u' => 'i',
                '/[óòôõºöðø]/u' => 'o',
                '/[ÓÒÔÕÖØ]/u' => 'O',
                '/[úùûüů]/u' => 'u',
                '/[ÚÙÛÜ]/u' => 'U',
                '/[ýýÿ]/u' => 'y',
                '/Š/' => 'S',
                '/š/' => 's',
				'/[Č?]/u'  => 'C',
                '/ç/' => 'c',
                '/Ç/' => 'C',
				'/č/' => 'c',
                '/Ð/' => 'Dj',
                '/ñ/' => 'n',
				'/ň/' => 'n',
				'/Ň/' => 'N',
                '/Ñ/' => 'N',
				'/Ť/' => 'T',
				'/ť/' =>'t',
				'/ř/' =>'r',
                '/Ý/' => 'Y',
                '/Ž/' => 'Z',
                '/ž/' => 'z',
                '/þ/' => 'b',
                '/Þ/' => 'B',
                '/ƒ/' => 'f',
                '/ß/' => 'ss',
                '/Œ/' => 'Oe',
                '/œ/' => 'oe',
                '/–/' => '-', // conversion d'un tiret UTF-8 en un tiret simple
                '/[‘’‚‹›]/u' => ' ', // guillemet simple
                '/[“”«»„]/u' => ' ', // guillemet double
                '/ /' => ' ' // espace insécable (équiv. à 0x160)
            );
            $text = preg_replace(array_keys($utf8), array_values($utf8), $text);
            return $text;
        } 
}

