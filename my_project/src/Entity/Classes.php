<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ClassesRepository")
 */
class Classes
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     */
    private $year;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Student", mappedBy="classes")
     */
    private $students;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\AnonymousId", mappedBy="classes")
     */
    private $anonymousIds;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TeacherClasses", mappedBy="classes")
     */
    private $teacherClasses;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\School", inversedBy="classes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $school;


    public function __construct()
    {
        $this->students = new ArrayCollection();
        $this->anonymousIds = new ArrayCollection();
        $this->teacherClasses = new ArrayCollection();
        $this->groups = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getYear(): ?int
    {
        return $this->year;
    }

    public function setYear(int $year): self
    {
        $this->year = $year;

        return $this;
    }

    /**
     * @return Collection|Student[]
     */
    public function getStudents(): Collection
    {
        return $this->students;
    }

    public function addStudent(Student $student): self
    {
        if (!$this->students->contains($student)) {
            $this->students[] = $student;
            $student->addClass($this);
        }

        return $this;
    }

    public function removeStudent(Student $student): self
    {
        if ($this->students->contains($student)) {
            $this->students->removeElement($student);
            $student->removeClass($this);
        }

        return $this;
    }

    /**
     * @return Collection|AnonymousId[]
     */
    public function getAnonymousIds(): Collection
    {
        return $this->anonymousIds;
    }

    public function addAnonymousId(AnonymousId $anonymousId): self
    {
        if (!$this->anonymousIds->contains($anonymousId)) {
            $this->anonymousIds[] = $anonymousId;
            $anonymousId->setClasses($this);
        }

        return $this;
    }

    public function removeAnonymousId(AnonymousId $anonymousId): self
    {
        if ($this->anonymousIds->contains($anonymousId)) {
            $this->anonymousIds->removeElement($anonymousId);
            // set the owning side to null (unless already changed)
            if ($anonymousId->getClasses() === $this) {
                $anonymousId->setClasses(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|TeacherClasses[]
     */
    public function getTeacherClasses(): Collection
    {
        return $this->teacherClasses;
    }

    public function addTeacherClass(TeacherClasses $teacherClass): self
    {
        if (!$this->teacherClasses->contains($teacherClass)) {
            $this->teacherClasses[] = $teacherClass;
            $teacherClass->setClasses($this);
        }

        return $this;
    }

    public function removeTeacherClass(TeacherClasses $teacherClass): self
    {
        if ($this->teacherClasses->contains($teacherClass)) {
            $this->teacherClasses->removeElement($teacherClass);
            // set the owning side to null (unless already changed)
            if ($teacherClass->getClasses() === $this) {
                $teacherClass->setClasses(null);
            }
        }

        return $this;
    }

    public function getSchool(): ?school
    {
        return $this->school;
    }

    public function setSchool(?school $school): self
    {
        $this->school = $school;

        return $this;
    }

   
}
