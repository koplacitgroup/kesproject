<?php

namespace App\Controller;

use App\Entity\Question;
use App\Form\QuestionType;
use App\Service\QuestionService;
use App\Repository\PageRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


/*This Controller is used manage question Entity Object in the database */ 
class QuestionController extends AbstractController
{
    /**
     * @Route("/question", name="question")
     */
    public function index()
    {  //Basic test page.
        
        return $this->render('question/index.html.twig', [
            'controller_name' => 'QuestionController',
        ]);
    }

    /**
     *@Route("/question/show/{id}", name="question_show")
     *
     */
    public function showQuestion(Question $question, $page_id= null, PageRepository $repopage){
         //Display a question (id, text,...).

        $page_id = $repopage->findOneByQuestion($question);
        
        
        return $this->render('question/show.html.twig',[
            'question' => $question, 'page_id' => $page_id["page_id"] ]);
    }

    /**
     * @Route("/question/create", name="question_create")
     * @Route("/question/update/{id}", name="question_update")
     * @Route("/admin/question/create_from_page/{page_id}", name="question_page_create")
     */
    public function createQuestion(Question $question=null,$page_id=null,Request $request,PageRepository $repopage,ObjectManager $manager)
    {   //Create or update a question.


        if(!$question)
        {   
            $question = new Question();

        }
            $form = $this->createForm(QuestionType::class,$question);
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
               $manager->persist($question);

               if($page_id){
                $page= $repopage->find($page_id);
                $page->addQuestion($question);
                $manager->persist($page);
                
            }
               $manager->flush();
               if(!$page_id)
                return $this->redirectToRoute('question_show',['id'=> $question->getId()]);
              else
                return $this->redirectToRoute('page_show',['id'=> $page_id]);
            }

            return $this->render('question/create.html.twig',['form'=> $form->createView()]);
        
        

    }


    /**
     * @Route("/question/delete/{id}", name="question_delete")
     * @Route("/question/delete/{id}/frompage/{page_id}", name="question_delete_page")
     */
    public function deleteQuestion(Question $question, $page_id=null,ObjectManager $manager,
    PageRepository $repopage, QuestionService $questionService){
       //Delete a question.
       
        if($question)
        {  

            if($page_id)
            {   $page = $repopage->find($page_id);
                $repopage->removeOneQuestion($page,$question);

                return $this->redirectToRoute('page_show',['id'=> $page_id]);
            }
           
                $questionService->deleteQuestion($question,$manager);
       
            
            
            

            
            return $this->render('page/delete.html.twig',['success' => 1]);
        }
        else {
            throw $this->createNotFoundException(
                'No page found for id '.$question->getId()
            );
        }
    }
}
