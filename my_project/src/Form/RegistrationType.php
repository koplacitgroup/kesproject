<?php

namespace App\Form;

use App\Entity\User;
use App\Entity\School;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;


//Form to register as a user.
class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('lastname')
            ->add('firstname')
            ->add('email')
            ->add('gender', ChoiceType::class, [
                'choices'  => [
                    'Male' => 'male',
                    'Female' =>'female'
                ],
                'expanded' => true,
                'multiple' =>false
              
            ])
            ->add('password',PasswordType::class)
            ->add('confirm_password', PasswordType::class)
            ->add('school',EntityType::class,[
                'class' => School::class,
                'choice_label' => 'name',
                'mapped' =>false
            ])
            
        ;
      
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
