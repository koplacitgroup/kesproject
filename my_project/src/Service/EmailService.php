<?php // src/Service/MessageGenerator.php
namespace App\Service;

use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class EmailService
{
    public function __construct(ObjectManager $manager,\Swift_Mailer $mailer, UserRepository $userRepository ,UserPasswordEncoderInterface $encoder)
{
    $this->manager = $manager;
    $this->mailer = $mailer;
    $this->userRepository = $userRepository;
    $this->encoder = $encoder;
}
    public function index($name, \Swift_Mailer $mailer)
    {

        
        $message = (new \Swift_Message('Hello Email'))
        ->setFrom('send@example.com')
        ->setTo('recipient@example.com')
        ->setBody(
            $this->renderView(
                // templates/emails/registration.html.twig
                'emails/registration.html.twig',
                ['name' => $name]
            ),
            'text/html'
        )

        // you can remove the following code if you don't define a text version for your emails
        ->addPart(
            $this->renderView(
                'emails/registration.txt.twig',
                ['name' => $name]
            ),
            'text/plain'
        )
    ;

        $mailer->send($message);

        return $this->render();
    }

    public function sendEmailtoTeacher($messageText, $email){
        $message = (new \Swift_Message('Result of the survey'))
        ->setFrom('noreply@kes.cz')
        ->setTo($email)
        ->setBody($messageText);
        $mailer->send($message);
    }

    public function changePassword( $email, $user){
        $password = $this->randomPassword();
        $hash = $this->encoder->encodePassword($user, $password);
        $user->setPassword($hash);
        $this->manager->persist($user);
        $this->manager->flush();
        $messageText = 'Your new password for email'.$email.  ': '.$password;
        $message = (new \Swift_Message('Your new password'))
        ->setFrom('noreply@kes.cz')
        ->setTo($email)
        ->setBody($messageText);
        $this->mailer->send($message);
    }

    public function randomPassword() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }
}