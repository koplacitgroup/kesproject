<?php

namespace App\Repository;

use App\Entity\Sheet;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\Config\Definition\Exception\Exception;

/**
 * @method Sheet|null find($id, $lockMode = null, $lockVersion = null)
 * @method Sheet|null findOneBy(array $criteria, array $orderBy = null)
 * @method Sheet[]    findAll()
 * @method Sheet[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SheetRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Sheet::class);
    }

    // /**
    //  * @return Sheet[] Returns an array of Sheet objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Sheet
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findBySchool($school){
        return $this->createQueryBuilder('s')
                    ->innerJoin('s.classes','c','WITH','c.school = :school')
                    ->setParameter('school', $school)
                    ->orderBy('s.classes')
                    ->getQuery()
                    ->getResult()
                    ;
    }


    public function findByDatePeriod($start_date , $end_date)
    {
        if($start_date > $end_date){
           return null;
            
        }else{
            return $this->createQueryBuilder('s')
            ->Where('s.createdAt >= :start_date')
            ->andWhere('s.createdAt <= :end_date')
            ->setParameters(array('start_date' => $start_date, 'end_date' => $end_date))
            ->orderBy('s.createdAt', 'DESC')
            ->getQuery()
            ->getResult()
        ;
        }

    
       
    }
    
}
