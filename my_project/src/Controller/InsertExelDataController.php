<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Classes;
use App\Entity\Student;
use App\Entity\Teacher;
use App\Entity\UserStudent;
use App\Entity\UserTeacher;
use App\Repository\SchoolRepository;
use App\Repository\ClassesRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class InsertExelDataController extends AbstractController 
{
    
    /**
     * @Route("/insert/exel/data", name="insert_exel_data")
     */
    public function index()
    { // Basic test page
        return $this->render('insert_exel_data/index.html.twig', [
            'controller_name' => 'InsertExelDataController',
        ]);
    }

/**
 * @Route("/insert/exel/data/teacher", name="isert_exel_teachers")
 */
    public function addTeachers(Request $request, ObjectManager $manager, UserPasswordEncoderInterface $encoder){
       
       // This page register the data from a CSV file that contains data about a teacher.
        $message="";
        
        if(isset($_FILES["file"]))
        {
            
            $fileName = $_FILES["file"]["tmp_name"];

            if($_FILES["file"]["size"] > 0)
            {
                $file =fopen($fileName,"r");
                $column=fgetcsv($file,10000,";");
                
                while(($column = fgetcsv($file,10000,";"))!==FALSE)
                {   
                    $teacher = new Teacher();
                    $userTeacher = new UserTeacher();
                    $teacher->setLastname($column[0]);
                    $teacher->setFirstname($column[1]);
                    $teacher->setGender($column[2]);
                    $teacher->setEmail($column[3]);
                    $user = new User();
                    $user->setFirstname($column[0]);
                    $user->setLastname($column[1]);
                    $user->setGender($column[2]);
                    $user->setEmail($column[3]);
                    $user->setSchool($this->getUser()->getSchool());
                    $user->setRoles(array('ROLE_TEACHER'));
                    $password = 'testtest';
                    $hash = $encoder->encodePassword($user, $password);
                    $user->setPassword($hash);

                    $userTeacher->setUser($user);
                    $userTeacher->setTeacher($teacher);
                    $user->setUserTeacher($userTeacher);
                    $user->setUserTeacher($userTeacher);
                    $manager->persist($user);
                    $manager->persist($teacher);
                    

                }
                $manager->flush();
                return $this->render('insert_exel_data/insert.teacher.submit.html.twig');
        
        }

        
        }
        return $this->render('insert_exel_data/insert.teacher.html.twig');
            
    }


    /**
 * @Route("/insert/exel/data/student", name="isert_exel_students")
 */
public function addStudents(Request $request, ObjectManager $manager,ClassesRepository $repoClasses){
 // This page register the data from a CSV file that contains data about a teacher.
 
    $message="";

   $school = $this->getUser()->getSchool();
    if(isset($_FILES["file"]))
    {
        
        $fileName = $_FILES["file"]["tmp_name"];

        if($_FILES["file"]["size"] > 0)
        {
            $file =fopen($fileName,"r");
            $column=fgetcsv($file,10000,";");
            
            while(($column = fgetcsv($file,10000,";"))!==FALSE)
            {   
                $student = new Student();
                $userStudent = new UserStudent();
                $student->setLastname($column[0]);
                $student->setFirstname($column[1]);
                $student->setEmail($column[3]);
               $classes =$repoClasses->findOneClassesByYearNameAndSchool($column[4],$column[3],$school);
               $student->addClass($classes);
               $user = new User();
               $user->setFirstname($column[0]);
               $user->setLastname($column[1]);
               $user->setGender($column[2]);
               $user->setEmail($column[3]);
               $user->setSchool($this->getUser()->getSchool());
               $user->setRoles(array('ROLE_STUDENT'));
               $password = 'testtest';
               $hash = $encoder->encodePassword($user, $password);
               $user->setPassword($hash);

               $userStudent->setUser($user);
               $userStudent->setStudent($student);
               $user->setUserStudent($userStudent);
               $manager->persist($user);

                $manager->persist($student);

            }
            $manager->flush();
            return $this->render('insert_exel_data/insert.student.submit.html.twig');
    
    }

    
    }
    return $this->render('insert_exel_data/insert.student.html.twig');
        
}

}

