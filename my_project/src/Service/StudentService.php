<?php
namespace App\Service;

use App\Entity\UserSheet;
use App\Entity\UserResponse;
use App\Repository\ResponseRepository;
use App\Repository\UserPageRepository;
use App\Repository\UserSheetRepository;
use App\Repository\UserResponseRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Session\SessionInterface;


class StudentService
{
    private $session;

    public function __construct(SessionInterface $session,
     UserResponseRepository $userResponseRepository,
      UserPageRepository $userPageRepository, ResponseRepository $responseRepository,
       ObjectManager $manager, UserSheetRepository $userSurveyRepository)
    {
        $this->session = $session;
        $this->userPageRepository = $userPageRepository;
        $this->manager = $manager;
        $this->userResponseRepository = $userResponseRepository;
        $this->responseRepository = $responseRepository;
        $this->userSurveyRepository = $userSurveyRepository;
    }

    public function someMethod()
    {
        // stores an attribute in the session for later reuse
        $this->session->set('attribute-name', 'attribute-value');

        // gets an attribute by name
        $foo = $this->session->get('foo');

        // the second argument is the value returned when the attribute doesn't exist
        $filters = $this->session->get('filters', []);

        // ...
    }

    public function isUserPageValid($anonymousId, $page){
        $userPage = $this->userPageRepository->findOneByAnonymousIdAndPage($anonymousId,$page);
        if($userPage){
            if($userPage->getValid()){
                return true;
            }
            else return false;
        }
        else {
            return 'NOT_FOUND';
        }
    }

    public function isUserPageExist($anonymousId, $page){
        $userPage = $this->userPageRepository->findOneByAnonymousIdAndPage($anonymousId,$page);
        if($userPage){
         
                return true;
            }
            else return false;
    }

    

    public function isCommentValid($comment,$min_char_number,$encoding){
        if(mb_strlen($comment, $encoding)> $min_char_number){
            return true;
        }
        else return false;
    }

    public function isStudentAlreadyResponded($anonymousId,$page){
        if($this->isUserPageExist($anonymousId,$page)){
            if($this->isUserPageValid($anonymousId,$page)){
                return 1;
            }
            else return 'RESPONDED_BUT_NOT_VALID';
            
        }
        return 0;
    }

    public function saveThePageSubmittedInDataBase($anonymousId,$respondedQuestions, $page){
            //This function save the data from a page the user filled up by a form in the DataBase.
        
        foreach ($respondedQuestions as $key => $respondedQuestion) {// This cycle verifying for each responses  form the User (anonmousId)if it is not already exist in the database.
           
            foreach ($respondedQuestion as $key => $responseSubmittedId) {
                
                $responseSubmitted = $this->responseRepository->find($responseSubmittedId);
                $userResponse; //Declare UserResponse Entity Object
                if($this->userResponseRepository->findOneBy(['page'=>$page, 'anonymousId'=>$anonymousId,'response'=>$responseSubmitted ])){
                    // if the response already exit we will update this response
                    $userResponse = $this->userResponseRepository->findOneBy(['page'=>$page, 'anonymousId'=>$anonymousId, 'response'=> $responseSubmitted]);
                }
                // if the response dont exist we will create a new response
                else $userResponse = new UserResponse();
    
                //Save the data from the form in the UserResponse Entity Object.
                $userResponse->setAnonymousId($anonymousId)
                             ->setPage($page)
                             ->setResponse($this->responseRepository->find($responseSubmitted));

                             $this->manager->persist($userResponse);
               
            }
             // Keep the UserReponses Entity objects in memory before add it to database 
             

            //Save all the UserResponses Entity Objects in the database
           
           
            
        }
            $this->manager->flush();
    }


    public function isAllUserPagesValid($anonymousId){
        $pages_selected=$anonymousId->getPagesSelected();
        foreach ($pages_selected as $key => $pgselectd) {
            $result= $this->userPageRepository->findOneByPage($pgselectd);
            if(!$result){
                return false;
            }
            elseif(!$result->getValid()){
                return false;
            }  
        }
        return true;

    }

    public function validTheSurvey($anonymousId){
    
       $userSurvey = new UserSheet();
        if ($this->isAllUserPagesValid($anonymousId)){
            
            if($this->userSurveyRepository->findOneByAnonymousId($anonymousId)){
                $userSurvey=$this->userSurveyRepository->findOneByAnonymousId($anonymousId);
                if(!$userSurvey->getValid()){
                    $userSurvey->setValid(true);
                }
            }else{
                $userSurvey->setValid(true)
                ->
                setAnonymousId($anonymousId)
                            ->setSheet($anonymousId->getSurvey())
                            
                            ;

            }
            
            
           

        $this->manager->persist($userSurvey);
        $this->manager->flush();

        return true;
        }

        return false;
       
    }

    
}

