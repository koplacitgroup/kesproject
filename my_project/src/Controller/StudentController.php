<?php

namespace App\Controller;

use App\Entity\Page;
use App\Entity\Sheet;
use App\Entity\Comment;
use App\Entity\Student;
use App\Entity\UserPage;
use App\Entity\AnonymousId;
use App\Entity\UserResponse;
use App\Service\SurveyService;
use App\Service\StudentService;
use App\Form\AnonymousLoginType;
use App\Repository\PageRepository;
use App\Service\DataUpdateService;
use App\Repository\SheetRepository;
use App\Repository\ClassesRepository;
use App\Repository\StudentRepository;
use App\Service\BadWordFilterService;
use App\Repository\ResponseRepository;
use App\Repository\UserPageRepository;
use App\Repository\AnonymousIdRepository;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\SurveyInProgressRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class StudentController extends AbstractController
{
    /**
     * @Route("/student/", name="student")
     */
    public function index(ClassesRepository $repoclasses, SurveyInProgressRepository $reposurveyinprogress, SheetRepository $reposurvey)
    {   
        // This is the route for the home page of a User type Student.
        
        $user =$this->getUser();
        $student = $user->getUserStudent()->getStudent();

        //Select the surveys of a Student in his actual class and he have to fill up and display them.
    $date = new \DateTime();
    $year = intVal($date->format('Y'));
        $classesId = $repoclasses->findOneByStudentAndYear($student,$year);
        $classes = $repoclasses->find($classesId['classes_id']);
       $surveyIds = $reposurveyinprogress->findByClasses($classes);
       $surveys=array();
       foreach ($surveyIds as $key => $srvId) {
           $surveys[]= $reposurvey->find($srvId['survey_id']);
       }
    

        

        return $this->render('student/index.html.twig', ['surveys'=>$surveys, 'school'=>$user->getSchool(),
        'classes' => $classes]);
    }
	
	/**
     * @Route("/studentcz/", name="studentcz")
     */
    public function indexcz(ClassesRepository $repoclasses, SurveyInProgressRepository $reposurveyinprogress, SheetRepository $reposurvey)
    {   
        // This is the route for the home page of a User type Student.
        
        $user =$this->getUser();
        $student = $user->getUserStudent()->getStudent();

        //Select the surveys of a Student in his actual class and he have to fill up and display them.
    $date = new \DateTime();
    $year = intVal($date->format('Y'));
        $classesId = $repoclasses->findOneByStudentAndYear($student,$year);
        $classes = $repoclasses->find($classesId['classes_id']);
       $surveyIds = $reposurveyinprogress->findByClasses($classes);
       $surveys=array();
       foreach ($surveyIds as $key => $srvId) {
           $surveys[]= $reposurvey->find($srvId['survey_id']);
       }
    

        

        return $this->render('student/indexcz.html.twig', ['surveys'=>$surveys, 'school'=>$user->getSchool(),
        'classes' => $classes]);
    }

    /**
     * @Route("/student/anonymous/login", name="student_anonymous_login")
     */
    public function studentAnonymousLogin(Request $request,AnonymousIdRepository $repoAnoymousId){
     /* Login anonymously to fill up a  survey with a login and a 
     password that the student recieve at school  */
       
       
        $form=$this->createForm(AnonymousLoginType::class);  // Create a form to display it in the web browser of the user
        $form->handleRequest($request);       //Control if the form was submitted

        if($form->isSubmitted() && $form->isValid()){//Condition is the when the form is submitted and the data in the form is correct
            
            //Collect data in the form       
            $login=$form->get('login')->getData();
            $password = $form->get('password')->getData();

            //Find the anoymousId  Entity Object ith the data the user have sent (login)
            $anonymousId= $repoAnoymousId->findOneById($login);
     if(!$anonymousId){
         return $this->render('/student/anonymous.login.html.twig',['form'=>$form->createView(),'message'=>'error the id and the password are invalid']);
     }
       // This condition is valid when the password and the login of the form is correct.     
       if($anonymousId != null && ($anonymousId->getPassword()==$password && $anonymousId->getValid()) ){

      
                    // Redirection to the survey the student need to fill up.
                    return $this->redirectToRoute('student_select_teacher_for_survey',['id'=> $anonymousId->getId()]);
                

       }
       //Redirection to the form to connect with the anonymousId and password with a message of error.
       elseif(!($anonymousId->getValid())){
        return  $this->render('/student/anonymous.login.html.twig',['form'=>$form->createView(),'message'=>'you have already fill up the survey']);
       }else{
        return  $this->render('/student/anonymous.login.html.twig',['form'=>$form->createView(),'message'=>'error the id and the password are invalid']);
       }  
        }

        //Display the login page to connect with the anonymousId and the password to access to the survey.
        return $this->render('/student/anonymous.login.html.twig',['form'=>$form->createView(),'message' => null]);
    }



    /**
     * @Route("student/survey_anonymousId/{id}", name="student_select_teacher_for_survey")
     */
    public function selectTeacherForSurvey(Request $request,AnonymousId $anonymousId, PageRepository $pageRepository, ObjectManager $manager, AnonymousIdRepository $anonymousIdRepository){
        if ($request->request->get('anonymousId')) {//Page before fill up the survey the student select the teacher he want to evaluate
            $anonymousId = $anonymousIdRepository->find($request->request->get('anonymousId'));
            $pages_selected = $request->request->get('pages_selected');
         
            $anonymousId->clearPagesSelected();
            foreach ($pages_selected as $key => $page_id) {
               
               $page = $pageRepository->find($page_id);

                if( ($page->getSheet())->getId() == (($anonymousId->getSurvey())->getId()) )
                {
                    
                    $anonymousId->addPagesSelected($page);
                }
                else{
                throw $this->createNotFoundException('Error in  SeletedTeacherForSurvey App/Controller Survey Controller :
                the pages selected are not in the survey');
                }
               
            }
            $manager->persist($anonymousId);
            $manager->flush();

                return $this->redirectToRoute('student_survey_page_fill_up_anonymousId',['id' => $anonymousId->getId(),'page_number' =>1]);
            
        }
        $survey = $anonymousId->getSurvey();
        $pages = $survey->getPages();
        $teachers = array();
        foreach ($pages as $key => $page) {
            $teachers[]=['page'=>$page->getId(),'teacher' => $page->getTeacher() , 'subject' =>$page->getSubject()];
            
        }
      
        return $this->render('/student/survey.choose.teacher.html.twig', ['teachers'=>$teachers, 'anonymousId'=>$anonymousId]);
    }

 /**
  * @Route("student/survey/fill_up_the_form/anonymousId/{id}/page_number/{page_number}", name="student_survey_page_fill_up_anonymousId")
  */
  public function showPageForm( AnonymousId $anonymousId,PageRepository $pageRepository, 
  $page_number,Request $request,ObjectManager $manager,DataUpdateService $dataUpdateService, 
  StudentService $studentService, SurveyService $surveyService,UserPageRepository $userPageRepository,
   BadWordFilterService $badwordfilterService){
    // This Route is used to show each page of the Survey to fill up the form  

      $number_of_pages = ($anonymousId->getPagesSelected())->count();
      if($request->request->get('form_submitted')){


                    // Collect all the data from the form 
                    $data_number_of_pages =intVal($request->request->get('number_of_pages'));
                    $data_page_number = intVal($request->request->get('page_number'));
                    $pageSubmitted = $pageRepository->find($request->request->get('pageid'));
                    $respondedQuestions = $request->request->get('responses');
                    $submittedComment = $request->request->get('comment');


        //Insert Data  of the form In DataBase
        
                // Verifying in the database if this page was not already filled up
                $studentAlreadyResponded = $studentService->isStudentAlreadyResponded($anonymousId,$pageSubmitted);

                if($studentAlreadyResponded === 1){
                    //the student has responded correcly to this page and it is valid
               

                }
                elseif($studentAlreadyResponded ===0 || $studentAlreadyResponded === 'RESPONDED_BUT_NOT_VALID' ) {
                        //The student never responded this this page
                        
                        $userPage = null;
                        if($studentAlreadyResponded === 'RESPONDED_BUT_NOT_VALID'){
                            $userPage = $userPageRepository->findOneBy(['anonymousId'=>$anonymousId, 'page'=>$pageSubmitted]);//Find the UserPage Entity to update it.
                        }
                        else{
                            $userPage = new UserPage();  // create a new UserPage Entity Object This object register if a anonymousId has responded correctly to the page.
                        }
                       
                        

                        //Save the Responses from the form filled up in the dataBase
                       
                     $studentService->saveThePageSubmittedInDataBase($anonymousId,$respondedQuestions, $pageSubmitted);

                     $userPage->setAnonymousId($anonymousId)
                              ->setPage($pageSubmitted)
                              ->setValid(true)
                              ->setMark($surveyService->getPageMarkFromAnonymousId($anonymousId,$pageSubmitted))
                              ;

                              //Verifying if the comment writed by the student is valid and if yes save it in the data base.
                              if($studentService->isCommentValid($submittedComment,10,'utf8') && !$badwordfilterService->isBadWordsExist($submittedComment)){
                                $comment = new Comment();
                                $comment->setText($submittedComment)
                                          ->setCreationDate(new \DateTime())
                                          ->setPage($pageSubmitted)
                                        ;
                                    $manager->persist($comment);
                            }
                            else{
                                $msg = 'comment not added';
                               
                            }
                    //Register that the student has filled up this page and if it is valid or not in the database.
                    $manager->persist($userPage);

                    //Desactivate the anonymousId to lock the userto response to the survey again
                            $anonymousId->setValid(false);
                    

                    //Save all the data persisted in the data base.
                    $manager->flush();

                }  

                //Update the data about the teacher marks.
                $dataUpdateService->updateTeacherMark($pageSubmitted);
        
        



                    //If Data correcly sended redirect to  the next page or the end of the form


                    //If there is an error during the saving of the data in the database send a error message
        



                    if($data_page_number < $data_number_of_pages){
                        //Redirect to the next page

                        return $this->render('student/page.show.html.twig',['pages'=>$anonymousId->getPagesSelected(),'anonymousId'=> $anonymousId,'number_of_pages'=> $data_number_of_pages, 'page_number'=>($data_page_number +1)]);
                    }
                    elseif($data_page_number = $data_number_of_pages){

                        //Redirect to the finish page
                    
                        $validationSurvey=$studentService->validTheSurvey($anonymousId);
                        if($validationSurvey){
                            return $this->render('student/survey.finished.html.twig',['message'=>'success']);
                        }
                        return $this->render('student/survey.finished.html.twig',['message'=>'error']);
                    }
                    else {
                    
                        
                        return $this->redirectToRoute('student/error.page.html.twig');
                    }
        }

      return $this->render('student/page.show.html.twig',['pages'=>$anonymousId->getPagesSelected(),'anonymousId'=> $anonymousId,'number_of_pages'=> $number_of_pages, 'page_number'=>$page_number]);

    }

}