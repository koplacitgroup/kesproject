<?php

namespace App\Repository;

use App\Entity\QuestionStandard;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method QuestionStandard|null find($id, $lockMode = null, $lockVersion = null)
 * @method QuestionStandard|null findOneBy(array $criteria, array $orderBy = null)
 * @method QuestionStandard[]    findAll()
 * @method QuestionStandard[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class QuestionStandardRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, QuestionStandard::class);
    }

    // /**
    //  * @return QuestionStandard[] Returns an array of QuestionStandard objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('q.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?QuestionStandard
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
