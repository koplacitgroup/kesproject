<?php
namespace App\Service;

use App\Repository\PageRepository;
use App\Repository\SheetRepository;
use App\Repository\TeacherRepository;
use App\Repository\UserPageRepository;
use App\Repository\TeacherClassesRepository;
use App\Repository\TeacherPageMarkRepository;


class TeacherService
{
    public function __construct(TeacherRepository $teacherRepository, 
    TeacherClassesRepository $teacherClassesRepository, SheetRepository $surveyRepository,
    PageRepository $pageRepository, UserPageRepository $userPageRepository,
    TeacherPageMarkRepository $teacherPageMarkRepository){
        $this->teacherRepository = $teacherRepository;
        $this->teacherClassesRepository = $teacherClassesRepository;
        $this->surveyRepository = $surveyRepository;
        $this->pageRepository   = $pageRepository;
        $this->userPageRepository = $userPageRepository;
        $this->teacherPageMarkRepository = $teacherPageMarkRepository;
    }




    //This function return the list of surveys of this year for a teacher and a year.
    public function getSurveysForTeacher($teacher,$year){
        $surveyList=array();
        $teacherClasses=$this->teacherClassesRepository->findByTeacherAndYear($teacher,$year);
        //List of teacherClasses object
        foreach ($teacherClasses as $key => $tcls) {
            
            $surveysFound = $this->surveyRepository->findByClasses($tcls->getClasses());
           
            foreach ($surveysFound as $key => $sf) {
              if($this->isTeacherInSurvey($sf,$teacher)){
                    $surveyList[]=$sf;
               }
                }
                    
            }

            return $surveyList;
        }
        
    

    public function isTeacherInSurvey($survey, $teacher){
        $pages = $survey->getPages();
        if($pages){
            foreach ($pages as $key => $page) {
                if($page->getTeacher($teacher)){
                    return true;
                }
                
            }
        }
         return false;
    }

    public function getTeacherPageFromSurvey($survey,$teacher){
        return $this->pageRepository->findByTeacherAndSurvey($survey, $teacher);
    }

    public function getPageBySurveys($surveyList, $teacher){
        $result= array();
        foreach ($surveyList as $key => $surv) {
            $result[]=['survey'=>$surv ,'pages'=> $this->getTeacherPageFromSurvey($surv,$teacher)];
        }

        return $result;
    }

    public function getMarkListofTeacherForOnePage($page){
          $userPages =$this->userPageRepository->findByPage($page);
          $result= array();
          if($userPages){
              foreach ($userPages as $key => $usrPg) {
                  $result[]=$usrPg->getMark();
              }

              return $result;
          }
          return null;
    }
    

    public function getMarksTeacherMainGraph($teacher,$page){
    $survey=$page->getSheet();
    $pages = $survey->getPages();
    $data = array();
    
    foreach ($pages as $key => $pg) {
        $result=$this->teacherPageMarkRepository->findOneByPage($pg) ;
        if ($result) {
            $data[]=['y'=>$result->getMark(), 'label'=>$pg->getSubject()->getName()];
        }
    }
    
return $data;
    }
    
}

