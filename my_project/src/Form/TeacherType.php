<?php

namespace App\Form;

use App\Entity\User;
use App\Entity\Subject;
use App\Entity\Teacher;
use App\Entity\School;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;


//Form to create a new teacher.
class TeacherType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('lastname')
            ->add('firstname')
            ->add('email')
            ->add('gender', ChoiceType::class, [
                'choices'  => [
                    'Male' => 'male',
                    'Female' =>'female'
                ],
                'expanded' => true,
                'multiple' =>false
            ])
            ->add('password', PasswordType::class)
            ->add('confirm_password', PasswordType::class)
            ->add('subjects',EntityType::class,
            [
                'class' => Subject::class,
                'choice_label' => 'name',
                'expanded' => false,
                'multiple' =>true,
                'mapped' =>false
            ])
            ->add('school',EntityType::class,
                   [ 'class'=>School::class,
                    'choice_label'=>'name'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
