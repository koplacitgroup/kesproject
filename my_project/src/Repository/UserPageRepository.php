<?php

namespace App\Repository;

use App\Entity\UserPage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method UserPage|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserPage|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserPage[]    findAll()
 * @method UserPage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserPageRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UserPage::class);
    }

    // /**
    //  * @return UserPage[] Returns an array of UserPage objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserPage
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    public function findOneByAnonymousIdAndPage($anonymousId, $page)  {
        return $this->createQueryBuilder('u')
            ->Where('u.page= :page')
            ->andWhere('u.anonymousId = :anonymousId')
            ->setParameters(array('anonymousId'=> $anonymousId,'page'=> $page))
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
}
