<?php

namespace App\Repository;

use App\Entity\TeacherClasses;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method TeacherClasses|null find($id, $lockMode = null, $lockVersion = null)
 * @method TeacherClasses|null findOneBy(array $criteria, array $orderBy = null)
 * @method TeacherClasses[]    findAll()
 * @method TeacherClasses[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TeacherClassesRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TeacherClasses::class);
    }

    // /**
    //  * @return TeacherClasses[] Returns an array of TeacherClasses objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TeacherClasses
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
public function findByTeacherAndClasses($teacher, $classes)
{
    return $this->createQueryBuilder('t')
        ->andWhere('t.teacher = :teacher')
        ->andWhere('t.classes = :classes')
        ->setParameter('teacher',$teacher)
        ->setParameter('classes', $classes)
        ->getQuery()
        ->getResult()
    ;
}

public function findByTeacherAndYear($teacher, $year){
    return $this->createQueryBuilder('tc')
        ->innerJoin('tc.classes', 'c', 'WITH', 'c.year = :year')
       ->Where('tc.teacher = :teacher')
        ->setParameter('teacher',$teacher)
        ->setParameter('year', $year)
        ->getQuery()
        ->getResult()
    ;
}



}
