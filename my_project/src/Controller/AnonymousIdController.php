<?php

namespace App\Controller;

use App\Entity\AnonymousId;
use App\Service\SurveyService;
use App\Repository\PageRepository;
use App\Repository\UserResponseRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AnonymousIdController extends AbstractController
{
    /**
     * @Route("/anonymous/id", name="anonymous_id")
     */
    public function index()
    {// A basic test page
        return $this->render('anonymous_id/index.html.twig', [
            'controller_name' => 'AnonymousIdController',
        ]);
    }


    /**
     * @Route("/anonymous/{id}/page/{page_id}", name="anonymous_id_page_mark")
     */
    public function getPageMarkForAnonymousId(AnonymousId $anonymousId, UserResponseRepository $userResponseRepository, $page_id, PageRepository $pageRepository,SurveyService $surveyService)
    { // Display the mark of a page that was filled up by a student anonymousLy.
        $page=$pageRepository->find($page_id);
        $pageMark=$surveyService->getPageMarkFromAnonymousId($anonymousId,$page);
        $userResponses = $userResponseRepository->findByPageAndAnonymousId($page, $anonymousId);
        return $this->render('anonymous_id/page.mark.html.twig', [
          'anonymousId'=>  $anonymousId,'pageMark' => $pageMark, 'page' => $page , 'userResponses' =>$userResponses
        ]);
    }
}
