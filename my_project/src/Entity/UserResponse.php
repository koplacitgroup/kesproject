<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserResponseRepository")
 */
class UserResponse
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\AnonymousId")
     * @ORM\JoinColumn(nullable=false)
     */
    private $anonymousId;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Page")
     * @ORM\JoinColumn(nullable=false)
     */
    private $page;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Response")
     * @ORM\JoinColumn(nullable=false)
     */
    private $response;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAnonymousId(): ?anonymousId
    {
        return $this->anonymousId;
    }

    public function setAnonymousId(?anonymousId $anonymousId): self
    {
        $this->anonymousId = $anonymousId;

        return $this;
    }

    public function getPage(): ?page
    {
        return $this->page;
    }

    public function setPage(?page $page): self
    {
        $this->page = $page;

        return $this;
    }

    public function getResponse(): ?response
    {
        return $this->response;
    }

    public function setResponse(?response $response): self
    {
        $this->response = $response;

        return $this;
    }
}
