<?php

namespace App\Repository;

use App\Entity\SurveyInProgress;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method SurveyInProgress|null find($id, $lockMode = null, $lockVersion = null)
 * @method SurveyInProgress|null findOneBy(array $criteria, array $orderBy = null)
 * @method SurveyInProgress[]    findAll()
 * @method SurveyInProgress[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SurveyInProgressRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, SurveyInProgress::class);
    }

    // /**
    //  * @return SurveyInProgress[] Returns an array of SurveyInProgress objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SurveyInProgress
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findByClasses($classes){
        $conn = $this->getEntityManager()->getConnection();
        $sql = 'SELECT survey_id FROM survey_in_progress sinp Inner Join sheet
        ON sinp.survey_id = sheet.id Where sheet.classes_id = :classesId';
        
        $stmt = $conn->prepare($sql);
    $stmt->execute(['classesId' => $classes->getId()]);

    // returns an array of arrays (i.e. a raw data set)
    return $stmt->fetchAll();
    }
}
