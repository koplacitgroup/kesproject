<?php

namespace App\Repository;

use App\Entity\UserValidSurvey;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method UserValidSurvey|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserValidSurvey|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserValidSurvey[]    findAll()
 * @method UserValidSurvey[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserValidSurveyRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UserValidSurvey::class);
    }

    // /**
    //  * @return UserValidSurvey[] Returns an array of UserValidSurvey objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserValidSurvey
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
