<?php

namespace App\Repository;

use App\Entity\Classes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Classes|null find($id, $lockMode = null, $lockVersion = null)
 * @method Classes|null findOneBy(array $criteria, array $orderBy = null)
 * @method Classes[]    findAll()
 * @method Classes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClassesRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Classes::class);
    }

    // /**
    //  * @return Classes[] Returns an array of Classes objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Classes
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findOneClassesByYearNameAndSchool($year,$name,$school){
        return $this->createQueryBuilder('c')
               ->andWhere('c.year = :year')
               ->andWhere('c.name = :name')
               ->andWhere('c.school = :school')
               ->setParameter('year',$year)
               ->setParameter('name' ,$name)
               ->setParameter('school', $school)
               ->getQuery()
               ->getOneOrNullResult()
               ;
    }

    public function findOneByStudentAndYear($student, $year){
        $conn = $this->getEntityManager()->getConnection();
        $sql = 'SELECT classes_id from student_classes stcl Inner Join classes
        ON stcl.classes_id = classes.id Where stcl.student_id = :studentId
        AND classes.year = :year';
        
        $stmt = $conn->prepare($sql);
    $stmt->execute(['studentId' => $student->getId(), 'year' =>$year ]);

    // returns an array of arrays (i.e. a raw data set)
    return $stmt->fetch();
    }
}
