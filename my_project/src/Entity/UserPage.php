<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserPageRepository")
 */
class UserPage
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Page")
     * @ORM\JoinColumn(nullable=false)
     */
    private $page;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\AnonymousId", inversedBy="userPages")
     * @ORM\JoinColumn(nullable=false)
     */
    private $anonymousId;

    /**
     * @ORM\Column(type="boolean")
     */
    private $valid;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $mark;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPage(): ?page
    {
        return $this->page;
    }

    public function setPage(?page $page): self
    {
        $this->page = $page;

        return $this;
    }

    public function getAnonymousId(): ?anonymousId
    {
        return $this->anonymousId;
    }

    public function setAnonymousId(?anonymousId $anonymousId): self
    {
        $this->anonymousId = $anonymousId;

        return $this;
    }

    public function getValid(): ?bool
    {
        return $this->valid;
    }

    public function setValid(bool $valid): self
    {
        $this->valid = $valid;

        return $this;
    }

    public function getMark(): ?float
    {
        return $this->mark;
    }

    public function setMark(?float $mark): self
    {
        $this->mark = $mark;

        return $this;
    }
}
