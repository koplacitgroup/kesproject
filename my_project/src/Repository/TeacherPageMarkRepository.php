<?php

namespace App\Repository;

use App\Entity\TeacherPageMark;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method TeacherPageMark|null find($id, $lockMode = null, $lockVersion = null)
 * @method TeacherPageMark|null findOneBy(array $criteria, array $orderBy = null)
 * @method TeacherPageMark[]    findAll()
 * @method TeacherPageMark[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TeacherPageMarkRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TeacherPageMark::class);
    }

    // /**
    //  * @return TeacherPageMark[] Returns an array of TeacherPageMark objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TeacherPageMark
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findOneByTeacherAndPage($teacher,$page): ?TeacherPageMark
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.teacher = :teacher')
            ->andWhere('t.page = :page')
            ->setParameter('teacher',$teacher)
            ->setParameter('page', $page)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
}
