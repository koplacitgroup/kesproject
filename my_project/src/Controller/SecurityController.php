<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\School;
use App\Entity\Classes;
use App\Entity\Student;
use App\Entity\Teacher;
use App\Form\StudentType;
use App\Form\TeacherType;
use App\Entity\AdminSchool;
use App\Entity\UserStudent;
use App\Entity\UserTeacher;
use App\Service\EmailService;
use App\Form\RegistrationType;
use App\Form\SchoolOfUserType;
use App\Repository\UserRepository;
use App\Form\ClassroomOfStudentType;
use App\Repository\ClassesRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class SecurityController extends AbstractController
{

    /**
     * @Route("/login", name="security_login")
     */
    public function connexion()
    {
        return $this->render('security/login.html.twig');
    }

    /**
    * @Route("/accessdenied", name="access_denied")
    */
    public function AccessDenied()
    {
        return $this->render('security/access.denied.html.twig');
    }

    /**
     * @Route("/logout" , name="security_logout")
     */
    public function logout(){}



/**
 * @Route("/registration/menu" , name="security_registration_menu")
 */
public function menu(){
//Route that will display a pag to select wich type of new user you want to register.

    return $this->render('security/registration.menu.html.twig');
}



    /**
     * @Route("/registration/959878", name="security_registration_user")
     */
    public function registration(Request $request, ObjectManager $manager,UserPasswordEncoderInterface $encoder){
         //Route to register ethier a admin  or a superAdmin or other
   
         $user = new User(); // Create the user entity object 

        $form = $this->createForm(RegistrationType::class,$user);//Create a form 
        $form->handleRequest($request);//Handle if the form was submitted
        if ($form->isSubmitted() && $form->isValid()) {//send the data of the form if the  data is correct
            

            // Set the data in the form to the User Entity Object :
                            // ****Set the role  of the user from the form submitted in the User Entity Object 
            $userRole = $request->request->get('role');
            $user->setRoles(array($userRole));

                        // *****Encode the password  from the form submitted and set the password in the User Entity Object 
            $hash = $encoder->encodePassword($user, $user->getPassword());//
           $user->setPassword($hash);

                        // **** Set the school Object form the form submitted in the User Entity object
           $user->setSchool($form->get('school')->getData());

                    // *** Keep the User Entity Object we have created and set up in the memory 
                    // before send it in the DataBase 
            $manager->persist($user); 
                  // **** Save the User Entity Object in the DataBase in the correct tables.
            $manager->flush();

            // Redirect to the login page
            return $this->redirectToRoute('security_login');
        }
               // display the form to fill up for registering a new user (admin or super admin)
        return $this->render('security/registration.html.twig',[
            'form' =>$form->createView()
        ]);
    }

    /**
     * @Route("/registration/959878cz", name="security_registration_usercz")
     */
    public function registrationcz(Request $request, ObjectManager $manager,UserPasswordEncoderInterface $encoder){
        //Route to register ethier a admin  or a superAdmin or other
  
        $user = new User(); // Create the user entity object 

       $form = $this->createForm(RegistrationType::class,$user);//Create a form 
       $form->handleRequest($request);//Handle if the form was submitted
       if ($form->isSubmitted() && $form->isValid()) {//send the data of the form if the  data is correct
           

           // Set the data in the form to the User Entity Object :
                           // ****Set the role  of the user from the form submitted in the User Entity Object 
           $userRole = $request->request->get('role');
           $user->setRoles(array($userRole));

                       // *****Encode the password  from the form submitted and set the password in the User Entity Object 
           $hash = $encoder->encodePassword($user, $user->getPassword());//
          $user->setPassword($hash);

                       // **** Set the school Object form the form submitted in the User Entity object
          $user->setSchool($form->get('school')->getData());

                   // *** Keep the User Entity Object we have created and set up in the memory 
                   // before send it in the DataBase 
           $manager->persist($user); 
                 // **** Save the User Entity Object in the DataBase in the correct tables.
           $manager->flush();

           // Redirect to the login page
           return $this->redirectToRoute('security_login');
       }
              // display the form to fill up for registering a new user (admin or super admin)
       return $this->render('security/cz/registrationcz.html.twig',[
           'form' =>$form->createView()
       ]);
   }

/**
 * @Route("/registration/student/getschool", name="security_registration_student_school")
 */
public function schoolForm(Request $request){

    //Display a form to select from wich school is the new student who is registering 
    $school = new School();

    $form = $this->createForm(SchoolOfUserType::class);
    

    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
        $school =$form->get('school')->getData();
        $classes =$school->getClasses();
        
        return $this->render('security/registration.student.classes.html.twig',[
            'form' =>$form->createView(),'classes' => $classes
            ]);
    }
    
    return $this->render('security/registration.getschool.html.twig',[
    'form' =>$form->createView(),'typeform' => 1
    ]);
}

     /**
     * @Route("/registration/student", name="security_registration_student")
     */
    public function studentRegistration(Request $request, ObjectManager $manager,UserPasswordEncoderInterface $encoder, ClassesRepository $repoclasse){
       //Display a form to register a new student in the DataBase after the school was selected in the previous form ( e.f the Route : security_registration_student_school )
       if(isset($_POST['classesId'])){
        $user = new User();
        $student = new Student();
        $userStudent = new UserStudent();
        $classes = $repoclasse->findOneById(($request->request->get('classesId')));
        $form = $this->createForm(StudentType::class,$user);
  
       
       
        
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {


          // Create the Student 
          $student->setLastname($user->getLastname());
        $student->setFirstname($user->getFirstname());
        $student->setEmail($user->getEmail());
        $classes = $repoclasse->findOneById(($request->request->get('classesId')));
        $student->addClass($classes);
        

         //Add the role student to the user
         $userRole = "ROLE_STUDENT";
         $user->setRoles(array($userRole));

        //encode User Password
        $hash = $encoder->encodePassword($user, $user->getPassword());
        $user->setPassword($hash);

        //add userSchool
        $user->setSchool($classes->getSchool());

        //Link user and student in the entity userStudent
        $userStudent->setStudent($student);
        $userStudent->setUser($user);
 
        //persit $user, $student and $userStudent
        $manager->persist($user);
        $manager->persist($student);
        $manager->persist($userStudent);

        //Insert all in the database
        $manager->flush();

        return $this->render('security/registration.student.finish.html.twig');
        }
   
        return $this->render('security/registration.student.html.twig',[
            'form' =>$form->createView(), 'classId' => $classes->getId()        
        ]);
       }
       else return $this->redirectToRoute('security_registration_student_school');
   
    }


    /**
    * @Route("/registration/teacher", name="security_registration_teacher")
    */
    public function teacherRegistration(Request $request, ObjectManager $manager,UserPasswordEncoderInterface $encoder){
       //Display a form to register a new teacher in the database.

        $teacher = new Teacher();
        $user = new User();
        
        $userTeacher = new UserTeacher();

        $form = $this->createForm(TeacherType::class,$user);

        //Listen if the form is subimitted.
        $form->handleRequest($request);
       
        //Condition if the form is submitted and valid add the data from the form to the data base.
        if ($form->isSubmitted() && $form->isValid()) {

          $teacher->setLastname($user->getLastname());
            $teacher->setFirstname($user->getFirstname());
            $teacher->setEmail($user->getEmail());
            foreach ($form->get('subjects')->getData() as $key => $subject) {
                $teacher->addSubject($subject);
            }
       
            
            

           $userRole = 'ROLE_TEACHER';
           $user->setRoles(array($userRole));

            //encode the User password.
        $hash = $encoder->encodePassword($user, $user->getPassword());
        $user->setPassword($hash);

           $userTeacher->setTeacher($teacher);
           $userTeacher->setUser($user);
           
           $manager->persist($user);
            $manager->persist($teacher);
            $manager->persist($userTeacher);
            $manager->flush();
 
            return $this->redirectToRoute('security_login');
        }

        return $this->render('security/registration.teacher.html.twig',[
            'form' =>$form->createView(),
            'userType' => 'Teacher'
        ]);
    }

    /**
    * @Route("/registration/cz/teachercz", name="security_registration_teachercz")
    */
    public function teacherRegistrationcz(Request $request, ObjectManager $manager,UserPasswordEncoderInterface $encoder){
        //Display a form to register a new teacher in the database.
 
         $teacher = new Teacher();
         $user = new User();
         
         $userTeacher = new UserTeacher();
 
         $form = $this->createForm(TeacherType::class,$user);
 
         //Listen if the form is subimitted.
         $form->handleRequest($request);
        
         //Condition if the form is submitted and valid add the data from the form to the data base.
         if ($form->isSubmitted() && $form->isValid()) {
 
           $teacher->setLastname($user->getLastname());
             $teacher->setFirstname($user->getFirstname());
             $teacher->setEmail($user->getEmail());
             foreach ($form->get('subjects')->getData() as $key => $subject) {
                 $teacher->addSubject($subject);
             }
        
             
             
 
            $userRole = 'ROLE_TEACHER';
            $user->setRoles(array($userRole));
 
             //encode the User password.
         $hash = $encoder->encodePassword($user, $user->getPassword());
         $user->setPassword($hash);
 
            $userTeacher->setTeacher($teacher);
            $userTeacher->setUser($user);
            
            $manager->persist($user);
             $manager->persist($teacher);
             $manager->persist($userTeacher);
             $manager->flush();
  
             return $this->redirectToRoute('security_login');
         }
 
         return $this->render('security/cz/registration.teachercz.html.twig',[
             'form' =>$form->createView(),
             'userType' => 'Teacher'
         ]);
    }

    /**
    * @Route("/changepassword", name="security_change_password")
    */
    public function ChangePass(Request $request,EmailService $emailService,\Swift_Mailer $mailer, ObjectManager $manager,UserRepository $userRepository, UserPasswordEncoderInterface $encoder)
    {   //Display a page for the User to change his password


            //if form was submitted :
        if ($request->request->get('emailtxt'))
        {
            //Collect data from the form
          //  $passwrd = $request->request->get('newpassword');
            //$confpass =$request->request->get('passrepeat');
            $email =$request->request->get('emailtxt');

            //Find if the user with this 'email' exists.
            $user = $userRepository->findOneByEmail($email);
            dump($user);

            //Verifying if the password input and the confirm pssword input are the same.
           /* if($passwrd == $confpass)
            {
                // Encoding the password in bcrypt
                $hash = $encoder->encodePassword($user, $passwrd);

                //Modifiying the user password after encoding.
                $user->setPassword($hash);
            }
*/
            //Update database:
        /*    $manager->persist($user);
            //Confirm the updating 
            $manager->flush();*/
$emailService->changePassword( $email ,$mailer,$user);

        }
        //Display the page to change the password.
        return $this->render('security/change.password.html.twig');
    }

    /**
    * @Route("/changepasswordcz", name="change_passwordcz")
    */
    public function ChangePassCZ(Request $request, ObjectManager $manager,UserRepository $userRepository, UserPasswordEncoderInterface $encoder)
    {   //Display a page for the User to change his password


            //if form was submitted :
        if ($request->request->get('emailtxt'))
        {
            //Collect data from the form
            $passwrd = $request->request->get('newpassword');
            $confpass =$request->request->get('passrepeat');
            $email =$request->request->get('emailtxt');

            //Find if the user with this 'email' exists.
            $user = $userRepository->findOneByEmail($email);

            //Verifying if the password input and the confirm pssword input are the same.
            if($passwrd == $confpass)
            {
                // Encoding the password in bcrypt
                $hash = $encoder->encodePassword($user, $passwrd);

                //Modifiying the user password after encoding.
                $user->setPassword($hash);
            }

            //Update database:
            $manager->persist($user);
            //Confirm the updating 
            $manager->flush();


        }
        //Display the page to change the password.
        return $this->render('security/cz/change.passwordcz.html.twig');
    }

    /**
    * @Route("/contactpage", name="contact_page")
    */
    public function ContactPage()
    {
        return $this->render('security/contact.html.twig');
    }

    /**
    * @Route("/aboutpage", name="about_page")
    */
    public function AboutPage()
    {
        return $this->render('security/about.html.twig');
    }

    /**
    * @Route("/logincz", name="login_cz")
    */
    public function CzechLogin()
    {
        return $this->render('security/logincz.html.twig');
    }

    /**
    * @Route("/aboutpagecz", name="about_pagecz")
    */
    public function AboutPageCZ()
    {
        return $this->render('security/aboutcz.html.twig');
    }

    /**
    * @Route("/contactpagecz", name="contact_pagecz")
    */
    public function ContactPageCZ()
    {
        return $this->render('security/contactcz.html.twig');
    }


      /**
    * @Route("/security/change_password/email", name="security_change_password_email")
    */
    public function changePasswordByEmail(Request $request,EmailService $emailService,UserRepository $userRepository){
        if($request->request->get('emailtxt')){
            $email = $request->request->get('emailtxt');
            $user = $userRepository->findOneByEmail($email);
            if($user){
                $emailService->changePassword($email,$user);
                return $this->render('security/change.password.email.html.twig', ['message' => 'mail sended']);
            }
        }
        return $this->render('security/change.password.email.html.twig',['message'=>null]);
    }
}