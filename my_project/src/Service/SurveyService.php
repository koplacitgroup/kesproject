<?php // src/Service/MessageGenerator.php
namespace App\Service;

use App\Entity\Page;
use App\Entity\Sheet;
use App\Entity\AnonymousId;
use App\Service\PageService;
use Psr\Log\LoggerInterface;
use App\Service\QuestionService;
use App\Repository\SheetRepository;
use App\Repository\ClassesRepository;
use App\Repository\QuestionRepository;
use App\Repository\UserResponseRepository;
use App\Repository\TeacherClassesRepository;
use App\Repository\QuestionStandardRepository;
use Doctrine\Common\Persistence\ObjectManager;
 
//This service contain function useful to do some setting on the survey.
class SurveyService
{
    public function __construct(PageService $pageService,QuestionRepository $questionRepository,SheetRepository $sheetRepository,QuestionService $questionService, UserResponseRepository $userResponseRepository,
    QuestionStandardRepository $questionStandardRepository, ClassesRepository $classesRepository,
    TeacherClassesRepository $teacherClassesRepository, ObjectManager $manager){
        $this->pageService                  = $pageService;
        $this->questionService              = $questionService;
        $this->questionStandardRepository   = $questionStandardRepository;
        $this->sheetRepository              = $sheetRepository;
        $this->userResponseRepository       = $userResponseRepository;
        $this->questionRepository           = $questionRepository;
        $this->classesRepository            = $classesRepository;
        $this->teacherClassesRepository     = $teacherClassesRepository;
        $this->manager                      = $manager;
    }


    public function isSurveyValid($survey) //this function is ask if the survey was created correctly
    {
        
        if (($this->sheetRepository->CountByPage())) {// count number of page in the survey
            $pages= $survey->getPages();
            foreach ($pages as $key => $page){
                if(!($this->pageService->isPageValid($page))) //For each page of the survey ask if the pages are correcly created
                return false;
            }
            return true;
        }

        return false;
    }

    public function deleteSurvey($survey,$manager){//This function delete a survey
                                                    //and every pages, questions, 
                                                    //and responses inside the survey.
        $pages = $survey->getPages();
        if(!empty($pages)){
            foreach ($pages as $key => $page) {
              $this->pageService->deletePage($page,$manager);
               
            }
           
        }
        
            $manager->remove($survey);

       
        $manager->flush();
    }


    public function createStandardSurvey($classes,$title, $repostdquest,$manager){
        // This function  create a survey automaticatilly with standard questions in the database.
        $survey = New Sheet();
        $survey->setClasses($classes);
        
        $teacherClasses=$this->teacherClassesRepository->findByClasses($classes);
        foreach ($teacherClasses as $key => $tcl) { 
            $page = New Page();
            $teacher = $tcl->getTeacher();
            $subject = $tcl->getSubject();
            $page->setTeacher($teacher);
            $page->setSubject($subject);
            $standardQuestionList = $repostdquest->findAll();
            foreach ($standardQuestionList as $key => $stdq) {
                $page->addQuestion($stdq->getQuestion());
            }
            $page->setSheet($survey);
            $manager->persist($page);
        }

        $survey->setSendable(true);
        $survey->setSended(false);
        $survey->setTitle($title);
        $survey->setCreatedAt(new \DateTime());
        $manager->persist($survey);
        $manager->flush();

    }

    public function getPageMarkFromAnonymousId($anonymousId,$page){
        //For one page filled up by one Student 
        //get the total result of the survey on 5 points.
     
    $survey=$anonymousId->getSurvey();

    $responseTable = array();
 
  
        $responseSum = 0;
       $userResponses = $this->userResponseRepository->findByPageAndAnonymousId($page, $anonymousId);
       foreach ($userResponses as $key => $usr) {
          $responseSum +=( $usr->getResponse())->getValue();
       }
       $questions = $page->getQuestions();
       $questionRateSum = 0;
       foreach ($questions as $key => $qust) {
           $result =$this->questionService->getQuestionRate($qust);
           $questionRateSum = $questionRateSum +$result;
           
       }

       return $pageMark = round(5*($responseSum/$questionRateSum),2);

    }


    
    public function generateAnonymousId($survey, $manager){
        //   This function gererate Anonymous Id automatically for a survey,
        //The number of anonymous Id will be the same as the number of student in the classroom. 
        
        $classes = $survey->getClasses();
        $nbr_of_student = ($classes->getStudents())->count();
        for ($i=1; $i <= $nbr_of_student ; $i++) { 
                $anonymousId = new AnonymousId();
                $anonymousId->setSurvey($survey)
                            ->setValid(true)
                            ->setPassword('testtest')
                            ->setCreatedAt(new \DateTime());
                $manager->persist($anonymousId);
        }
        
        $manager->flush();
        return $survey->getAnonymousIds();

        
        
    }


    public function createSurveyForAllSchool($school, $year, $auto, $questionList){
        if($auto == true){
           
            $classesList = $this->classesRepository->findBy(['school'=> $school, 'year'=> $year]);
            $repostdquest = $this->questionStandardRepository;
            $manager = $this->manager;
            foreach ($classesList as $key => $cls) {
                $title = 'Survey of the class '.$cls->getName().' '.$cls->getYear();
               
                $this->createStandardSurvey($cls,$title, $repostdquest,$manager);
            }
        }
        elseif ($auto  == false) {
            $classesList = $this->classesRepository->findBy(['school'=> $school, 'year'=> $year]);
            //$repostdquest = $this->questionStandardRepository;
            $manager = $this->manager;
            foreach ($classesList as $key => $cls) {
                $title = 'Survey of the class '.$cls->getName().' '.$cls->getYear();
               
                $this->createSurveyWithQuestions($cls,$questionList,$title);
            }   
        }
    
    }

    public function createSurveyForClasses($classesList, $auto, $questionList){
    
        if($auto == true)
        {
            $repostdquest = $this->questionStandardRepository;
            $manager = $this->manager;
            foreach ($classesList as $key => $cls) {
                $title = 'Survey of the class '.$cls->getName().' '.$cls->getYear();
               
               $this->createStandardSurvey($cls,$title, $repostdquest,$manager);
            
            
            }
        }
        if($auto == false){
           // $repostdquest = $this->questionStandardRepository;
            
            foreach ($classesList as $key => $cls) {
                $title = 'Survey of the class '.$cls->getName().' '.$cls->getYear();
               
                $this->createSurveyWithQuestions($cls,$questionList,$title);
            
            
            }
        }
       
    }

    

    public function createSurveyWithQuestions($classes,$questionList,$title){
        // This function  create a survey automaticatilly with  questions inside  "$questionList".
        $survey = New Sheet();
        $survey->setClasses($classes);
        
        $teacherClasses=$this->teacherClassesRepository->findByClasses($classes);
        foreach ($teacherClasses as $key => $tcl) { 
            $page = New Page();
            $teacher = $tcl->getTeacher();
            $subject = $tcl->getSubject();
            $page->setTeacher($teacher);
            $page->setSubject($subject);
        
            foreach ($questionList as $key => $quest) {
                $page->addQuestion($quest);
            }
            $page->setSheet($survey);
            $this->manager->persist($page);
        }

        $survey->setSendable(true);
        $survey->setSended(false);
        $survey->setTitle($title);
        $survey->setCreatedAt(new \DateTime());
        $this->manager->persist($survey);
        $this->manager->flush();

    }   


    public function createSurveyFromSurveyGroup($surveyGroup){
        // This function  create a survey automaticatilly with  questions inside  "$questionList".
        $surveyList = $surveyGroup->getSurveys();
        $questionList = $surveyGroup->getQuestions();
        
        foreach ($surveyList as $key => $survey) {
            $classes = $survey->getClasses();
        
        $teacherClasses=$this->teacherClassesRepository->findByClasses($classes);
        foreach ($teacherClasses as $key => $tcl) { 
            $page = New Page();
            $teacher = $tcl->getTeacher();
            $subject = $tcl->getSubject();
            $page->setTeacher($teacher);
            $page->setSubject($subject);
        
            foreach ($questionList as $key => $quest) {
                $page->addQuestion($quest);
            }
            $page->setSheet($survey);
            $this->manager->persist($page);
        }
        $title = 'Survey of class '.$classes->getName().' '.$classes->getYear();
        $survey->setSendable(true);
        $survey->setSended(false);
        $survey->setTitle($title);
        $survey->setCreatedAt(new \DateTime());
        $this->manager->persist($survey);    
        }
        



        $this->manager->flush();

    }   
}