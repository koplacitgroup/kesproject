<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AnonymousIdRepository")
 */
class AnonymousId
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

  

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="boolean")
     */
    private $valid;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $link;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserPage", mappedBy="anonymousId")
     */
    private $userPages;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Sheet", inversedBy="anonymousIds")
     * @ORM\JoinColumn(nullable=false)
     */
    private $survey;

  
    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Page")
     */
    private $pages_selected;

    public function __construct()
    {
        
        $this->pages_selected = new ArrayCollection();
    }

  
   
    

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getClasses(): ?classes
    {
        return $this->classes;
    }

    public function setClasses(?classes $classes): self
    {
        $this->classes = $classes;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getValid(): ?bool
    {
        return $this->valid;
    }

    public function setValid(bool $valid): self
    {
        $this->valid = $valid;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(?string $link): self
    {
        $this->link = $link;

        return $this;
    }

    /**
     * @return Collection|UserPage[]
     */
    public function getUserPages(): Collection
    {
        return $this->userPages;
    }

    public function addUserPage(UserPage $userPage): self
    {
        if (!$this->userPages->contains($userPage)) {
            $this->userPages[] = $userPage;
            $userPage->setAnonymousId($this);
        }

        return $this;
    }

    public function removeUserPage(UserPage $userPage): self
    {
        if ($this->userPages->contains($userPage)) {
            $this->userPages->removeElement($userPage);
            // set the owning side to null (unless already changed)
            if ($userPage->getAnonymousId() === $this) {
                $userPage->setAnonymousId(null);
            }
        }

        return $this;
    }

    public function getSurvey(): ?Sheet
    {
        return $this->survey;
    }

    public function setSurvey(?Sheet $survey): self
    {
        $this->survey = $survey;

        return $this;
    }

    /**
     * @return Collection|Page[]
     */
    public function getPagesSelected(): Collection
    {
        return $this->pages_selected;
    }

    public function addPagesSelected(Page $pagesSelected): self
    {
        if (!$this->pages_selected->contains($pagesSelected)) {
            $this->pages_selected[] = $pagesSelected;
        }

        return $this;
    }

    public function removePagesSelected(Page $pagesSelected): self
    {
        if ($this->pages_selected->contains($pagesSelected)) {
            $this->pages_selected->removeElement($pagesSelected);
        }

        return $this;
    }

    public function clearPagesSelected(): self
    {
        $this->pages_selected->clear();
        return $this;
    }

}
