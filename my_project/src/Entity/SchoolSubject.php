<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SchoolSubjectRepository")
 */
class SchoolSubject
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $subject_name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\School", inversedBy="schoolSubjects")
     * @ORM\JoinColumn(nullable=false)
     */
    private $school;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $subject_code;

   
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TeacherClasses", mappedBy="school_subject")
     */
    private $teacherClasses;

    public function __construct()
    {
        $this->Groups = new ArrayCollection();
        $this->teacherClasses = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSubjectName(): ?string
    {
        return $this->subject_name;
    }

    public function setSubjectName(string $subject_name): self
    {
        $this->subject_name = $subject_name;

        return $this;
    }

    public function getSchool(): ?School
    {
        return $this->school;
    }

    public function setSchool(?School $school): self
    {
        $this->school = $school;

        return $this;
    }

    public function getSubjectCode(): ?string
    {
        return $this->subject_code;
    }

    public function setSubjectCode(string $subject_code): self
    {
        $this->subject_code = $subject_code;

        return $this;
    }

   

    /**
     * @return Collection|TeacherClasses[]
     */
    public function getTeacherClasses(): Collection
    {
        return $this->teacherClasses;
    }

    public function addTeacherClass(TeacherClasses $teacherClass): self
    {
        if (!$this->teacherClasses->contains($teacherClass)) {
            $this->teacherClasses[] = $teacherClass;
            $teacherClass->setSchoolSubject($this);
        }

        return $this;
    }

    public function removeTeacherClass(TeacherClasses $teacherClass): self
    {
        if ($this->teacherClasses->contains($teacherClass)) {
            $this->teacherClasses->removeElement($teacherClass);
            // set the owning side to null (unless already changed)
            if ($teacherClass->getSchoolSubject() === $this) {
                $teacherClass->setSchoolSubject(null);
            }
        }

        return $this;
    }
}
