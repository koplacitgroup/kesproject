<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SchoolRepository")
 */
class School
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $academy;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Classes", mappedBy="school")
     */
    private $classes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\User", mappedBy="school")
     */
    private $users;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Subject", mappedBy="schools")
     */
    private $subjects;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SchoolSubject", mappedBy="school")
     */
    private $schoolSubjects;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SurveyGroup", mappedBy="school")
     */
    private $surveyGroups;


    public function __construct()
    {
        $this->classes = new ArrayCollection();
        $this->users = new ArrayCollection();
        $this->subjects = new ArrayCollection();
        $this->schoolSubjects = new ArrayCollection();
        $this->surveyGroups = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getAcademy(): ?string
    {
        return $this->academy;
    }

    public function setAcademy(string $academy): self
    {
        $this->academy = $academy;

        return $this;
    }

    /**
     * @return Collection|Classes[]
     */
    public function getClasses(): Collection
    {
        return $this->classes;
    }

    public function addClass(Classes $class): self
    {
        if (!$this->classes->contains($class)) {
            $this->classes[] = $class;
            $class->setSchool($this);
        }

        return $this;
    }

    public function removeClass(Classes $class): self
    {
        if ($this->classes->contains($class)) {
            $this->classes->removeElement($class);
            // set the owning side to null (unless already changed)
            if ($class->getSchool() === $this) {
                $class->setSchool(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->setSchool($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
            // set the owning side to null (unless already changed)
            if ($user->getSchool() === $this) {
                $user->setSchool(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|SchoolSubject[]
     */
    public function getSchoolSubjects(): Collection
    {
        return $this->schoolSubjects;
    }

    public function addSchoolSubject(SchoolSubject $schoolSubject): self
    {
        if (!$this->schoolSubjects->contains($schoolSubject)) {
            $this->schoolSubjects[] = $schoolSubject;
            $schoolSubject->setSchool($this);
        }

        return $this;
    }

    public function removeSchoolSubject(SchoolSubject $schoolSubject): self
    {
        if ($this->schoolSubjects->contains($schoolSubject)) {
            $this->schoolSubjects->removeElement($schoolSubject);
            // set the owning side to null (unless already changed)
            if ($schoolSubject->getSchool() === $this) {
                $schoolSubject->setSchool(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|SurveyGroup[]
     */
    public function getSurveyGroups(): Collection
    {
        return $this->surveyGroups;
    }

    public function addSurveyGroup(SurveyGroup $surveyGroup): self
    {
        if (!$this->surveyGroups->contains($surveyGroup)) {
            $this->surveyGroups[] = $surveyGroup;
            $surveyGroup->setSchool($this);
        }

        return $this;
    }

    public function removeSurveyGroup(SurveyGroup $surveyGroup): self
    {
        if ($this->surveyGroups->contains($surveyGroup)) {
            $this->surveyGroups->removeElement($surveyGroup);
            // set the owning side to null (unless already changed)
            if ($surveyGroup->getSchool() === $this) {
                $surveyGroup->setSchool(null);
            }
        }

        return $this;
    }

}
