<?php

namespace App\Controller;

use App\Entity\Question;
use App\Entity\Response;
use App\Form\ResponseType;
use App\Service\SurveyService;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ResponseController extends AbstractController
{
    /**
     * @Route("/response", name="response")
     */
    public function index()
    {// Basic test page.
        return $this->render('response/index.html.twig', [
            'controller_name' => 'ResponseController',
        ]);
    }

    /**
     *@Route("/response/show/{id}", name="response_show")
     */
    public function showResponse(Response $response){
     //Display one response with its id.
     
        return $this->render('response/show.html.twig',[
            'response' => $response
        ]);
    }

    /**
     * @Route("/response/delete/{id}", name= "response_delete")
     */
    public function deleteResponse(Response $response, ObjectManager $manager){
        //Delete one response.
        
        if($response)
        {
            $questionId = ($response->getQuestion())->getId();
            $manager->remove($response);
            $manager->flush();
            return $this->render('response/delete.html.twig',['success' => 1, 'questionId'=>$questionId]);
        }
        else{   
            throw $this->createNotFoundException(

                //Error if the response was not found.
                'No page found for id '.$reponse->getId()
            );

        }

    }


    /**    
     *@Route("/response/create/{id}/from_page/{page_id}", name="response_create")
     * @Route("/response/create/{id}", name="response_create_from_question")
    */
    public function createResponse(Question $question=null,$page_id=null,ObjectManager $manager, Request $request,
    SurveyService $surveyService)
    { //Create a response for a question.  
            if($question)
            {
                $response = new Response();
                $response->setQuestion($question);
            }
            $form = $this->createForm(ResponseType::class, $response);
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $manager->persist($response);
                $manager->flush();
               
                return $this->render('response/show.html.twig', ['response' => $response, 'page_id'=>$page_id]);
            }

            return $this->render('response/create.html.twig', ['form' =>$form->createView()]);

    } 

    /**    
     *@Route("/response/update/{id}", name="response_update")
    */
    public function updateResponse(Response $response=null,ObjectManager $manager, Request $request)
    {   //Update a response.

            if($response)
            { 
                
                $form = $this->createForm(ResponseType::class, $response);
                $form->handleRequest($request);
                if ($form->isSubmitted() && $form->isValid()) {
                    $manager->persist($response);
                    $manager->flush();
    
                    return $this->render('response/show.html.twig', ['response' => $response]);
                }
                return $this->render('response/create.html.twig', ['form' =>$form->createView()]); 
            }
            else{   
                throw $this->createNotFoundException(
                    'No page found for id '.$reponse->getId()
                );
    
            }
          
            

    } 
}
