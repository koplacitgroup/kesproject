<?php

namespace App\Controller;

use App\Entity\Classes;
use App\Entity\Websitecounter;
use App\Repository\TeacherRepository;
use App\Repository\WebsitecounterRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(ObjectManager $manager, WebsitecounterRepository $websitecounterRepository)
    {
        $websitecounter=$websitecounterRepository->find(1);
        $counts=$websitecounter->getCount() +1;
        $websitecounter->setCount($counts);
        $manager->persist($websitecounter);
        $manager->flush();
        // usually you'll want to make sure the user is authenticated first
    $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

    // returns your User object, or null if the user is not authenticated
    // use inline documentation to tell your editor your exact User class
    /** @var \App\Entity\User $user */
    $user = $this->getUser();
    if($this->isGranted('ROLE_ADMIN')) //redirect repectively to the home page of the User following his role.
        return $this->redirectToRoute('admin');
    elseif($this->isGranted('ROLE_STUDENT'))
        return $this->redirectToRoute('student');
    elseif($this->isGranted('ROLE_TEACHER'))
        return $this->redirectToRoute('teacher');
    elseif($this->isGranted('ROLE_SUPER_ADMIN'))
         return $this->redirectToRoute('super_admin');
    else
    {

        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }

        
    }

    
}
