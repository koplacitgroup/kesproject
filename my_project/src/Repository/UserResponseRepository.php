<?php

namespace App\Repository;

use App\Entity\UserResponse;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method UserResponse|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserResponse|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserResponse[]    findAll()
 * @method UserResponse[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserResponseRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UserResponse::class);
    }

    // /**
    //  * @return UserResponse[] Returns an array of UserResponse objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserResponse
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findByAnonymousIdAndSurvey($anonymousId,$survey){
        return $this->createQueryBuilder('u')
            ->InnerJoin('u.page','p','WITH','p.sheet = :survey')
            ->where('u.anonymousId = :anonymousId')
            ->setParameters(array('survey'=>$survey, 'anonymousId'=>$anonymousId))
            ->getQuery()
            ->getResult()
            ;

    }

    public function findByPageAndAnonymousId($page, $anonymousId){
        return $this->createQueryBuilder('u')
                    ->where('u.anonymousId = :anonymousId')
                    ->andWhere('u.page = :page')
                    ->setParameters(array('page'=>$page, 'anonymousId'=>$anonymousId))
                    ->getQuery()
                    ->getResult()
                    ;
    }

    public function findBySurveyAndTeacher($survey, $teacher){

        return $this->createQueryBuilder('u')
        ->InnerJoin('u.page','p','WITH','p.sheet = :survey')
        ->InnerJoin('u.page', 'v','WITH', 'v.teacher =  :teacher')
        ->setParameters(array('survey'=>$survey, 'teacher'=>$teacher))
        ->getQuery()
        ->getResult()
        ;
    }

   /* public function findByPageAnonymousIdAndQuestion($page,$anonymousId,$question){
        ret
    }*/
}
