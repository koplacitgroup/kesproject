<?php

namespace App\Repository;

use App\Entity\AnonymousId;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method AnonymousId|null find($id, $lockMode = null, $lockVersion = null)
 * @method AnonymousId|null findOneBy(array $criteria, array $orderBy = null)
 * @method AnonymousId[]    findAll()
 * @method AnonymousId[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AnonymousIdRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, AnonymousId::class);
    }

    // /**
    //  * @return AnonymousId[] Returns an array of AnonymousId objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AnonymousId
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
