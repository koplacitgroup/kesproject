<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserSheetRepository")
 */
class UserSheet
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\AnonymousId", inversedBy="userSheets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $anonymousId;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Sheet")
     * @ORM\JoinColumn(nullable=false)
     */
    private $sheet;

    /**
     * @ORM\Column(type="boolean")
     */
    private $valid;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAnonymousId(): ?anonymousId
    {
        return $this->anonymousId;
    }

    public function setAnonymousId(?anonymousId $anonymousId): self
    {
        $this->anonymousId = $anonymousId;

        return $this;
    }

    public function getSheet(): ?sheet
    {
        return $this->sheet;
    }

    public function setSheet(?sheet $sheet): self
    {
        $this->sheet = $sheet;

        return $this;
    }

    public function getValid(): ?bool
    {
        return $this->valid;
    }

    public function setValid(bool $valid): self
    {
        $this->valid = $valid;

        return $this;
    }
}
