<?php

namespace App\Controller;



use App\Entity\School;
use App\Form\SchoolType;
use App\Entity\SchoolSubject;
use App\Form\SchoolSubjectType;
use App\Service\SuperAdminService;
use App\Repository\SchoolRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SuperAdminController extends AbstractController
{
    /**
     * @Route("/superAdmin/", name="super_admin")
     */
    public function index()
    {
    

        return $this->render('superadmin/index.html.twig',['local'=>null]);

        
    }

        /**
     * @Route("/superAdmin/create_school", name="super_admin_create_school")
     * @Route("/superAdmin/update_school/{id}", name="super_admin_update_school")
     */
    public function createSchool(School $school=null, Request $request, ObjectManager $manager)
    {
        $mode = 'update';
        if(!$school){
            $school = new School();
            $mode= 'create';
        }
        $form = $this->createForm(SchoolType::class,$school);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $manager->persist($school);
            $manager->flush();
            $mode='form_submitted';

            return $this->render('superadmin/create.school.html.twig',['form'=>$form->createView(),'mode'=>$mode]);
        }

        return $this->render('superadmin/create.school.html.twig',['form'=>$form->createView(), 'mode'=>$mode]);

    }


    /**
     * @Route("superAdmin/insert_classes",name="super_admin_insert_classes")
     */
    public function insertClasses(SuperAdminService $superAdminService,Request $request, SchoolRepository $schoolRepository){
        if($request->request->get('school_selected')){
            $school = $schoolRepository->find($request->request->get('school_selected'));
            if($school){
                $superAdminService->insertClasses("file", $school);
            }
            return $this->render('superadmin/insert.classes.finish.html.twig');
        }

        $schoolList = $schoolRepository->findAll();
        return $this->render('superadmin/insert.classes.html.twig',['schoolList' =>$schoolList]);

    }
    
    /**
     * @Route("superAdmin/insert_teacher",name="super_admin_insert_teachers")
     */
    public function insertTeachers(SuperAdminService $superAdminService,Request $request, SchoolRepository $schoolRepository){

        if($request->request->get('school_selected')){
            $school = $schoolRepository->find($request->request->get('school_selected'));
            if($school){
                $superAdminService->insertTeachers("file", $school);
            }
            return $this->render('superadmin/insert.teachers.finish.html.twig');
        }

        $schoolList = $schoolRepository->findAll();
        return $this->render('superadmin/insert.teachers.html.twig',['schoolList' =>$schoolList]);

    }

    /**
     * @Route("superAdmin/insert_schoolsubject",name="super_admin_insert_schoolsubject")
     * @Route("superAdmin/update_schoolsubject/{id}", name="super_admin_update_schoolsubject")
     */
    public function insertSchoolSubject(SchoolSubject $schoolSubject=null, ObjectManager $manager,Request $request){
        if(!$schoolSubject){
            $schoolSubject = new SchoolSubject();
        }
        $form = $this->createForm(SchoolSubjectType::class,$schoolSubject);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $manager->persist($schoolSubject);
            $manager->flush();

            return $this->render('superadmin/insert.subject.html.twig',['form'=>null, 'message'=>'data inserted']);
        }


        return $this->render('superadmin/insert.subject.html.twig',['form'=>$form->createView()]);
    }

    /**
     * @Route("superAdmin/insert_list_schoolsubject", name="super_admin_insert_list_schoolsubject")
     */
    public function insertListSchoolSubject(SuperAdminService $superAdminService, Request $request, SchoolRepository $schoolRepository){

        if($request->request->get('school_selected')){
            $school = $schoolRepository->find($request->request->get('school_selected'));
            if($school){
                $superAdminService->insertSchoolSubjectsCsv("file", $school,true);
            }
            return $this->render('superadmin/insert.list.subject.finish.html.twig');
        }

        $schoolList = $schoolRepository->findAll();
        return $this->render('superadmin/insert.list.subject.html.twig',['schoolList' =>$schoolList]);
    }
}
