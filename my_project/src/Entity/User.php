<?php

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string",length=255)
     * @Assert\Length(min="8", minMessage="Your password has to have 8 character minimum")
     */
    private $password;

    /**
     * @Assert\EqualTo(propertyPath="confirm_password", message="You have not type the same password")
     */
    public $confirm_password;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $firstname;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\UserTeacher", mappedBy="user", cascade={"persist", "remove"})
     */
    private $userTeacher;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\UserStudent", mappedBy="user", cascade={"persist", "remove"})
     */
    private $userStudent;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\School", inversedBy="users")
     */
    private $school;

    /**
     * @ORM\Column(type="string", length=6)
     */
    private $gender;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

  

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getUserTeacher(): ?UserTeacher
    {
        return $this->userTeacher;
    }

    public function setUserTeacher(UserTeacher $userTeacher): self
    {
        $this->userTeacher = $userTeacher;

        // set the owning side of the relation if necessary
        if ($this !== $userTeacher->getUser()) {
            $userTeacher->setUser($this);
        }

        return $this;
    }

    public function getUserStudent(): ?UserStudent
    {
        return $this->userStudent;
    }

    public function setUserStudent(UserStudent $userStudent): self
    {
        $this->userStudent = $userStudent;

        // set the owning side of the relation if necessary
        if ($this !== $userStudent->getUser()) {
            $userStudent->setUser($this);
        }

        return $this;
    }

    public function getSchool(): ?School
    {
        return $this->school;
    }

    public function setSchool(?School $school): self
    {
        $this->school = $school;

        return $this;
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setGender(string $gender): self
    {
        $this->gender = $gender;

        return $this;
    }
}
