<?php

namespace App\Controller;

use App\Entity\Sheet;
use App\Entity\School;
use App\Entity\Classes;
use App\Service\SurveyService;
use App\Repository\SheetRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;

class SheetController extends AbstractController
{
    /**
     * @Route("/sheet", name="sheet")
     */
    public function index()
    { //Basic test page.
        return $this->render('sheet/index.html.twig', [
            'controller_name' => 'SheetController',
        ]);
    }

    /**
     * @Route("survey/delete/{id}", name="survey_delete")
     */
    public function surveyDelete(Sheet $survey,SurveyService $surveyService,ObjectManager $manager){
        //This page is used to delete a survey from a id refered to "{id}".
        
        if ($survey) {
            //use a service to delete survey and everything linked to it that need to be deleted.
            $surveyService->deleteSurvey($survey,$manager);
            return $this->render('sheet/delete.html.twig');
        }
        else {
            throw $this->createNotFoundException(

                //Thow a error if the survey was not found.
                'No page found for id '.$survey->getId()
            );
        }
    }

      /**
     * @Route("survey/deleteAll/school/{id}", name="survey_delete_all")
     */
    public function surveyDeleteAll(School $school,SurveyService $surveyService,SheetRepository $surveyRepository,ObjectManager $manager){
    //Delete all the surveys from a school.
    
    
        if ($school) {
            $surveys =$surveyRepository->findBySchool($school);
            foreach ($surveys as $key => $survey) {
                $surveyService->deleteSurvey($survey,$manager);
            }
         
            return $this->render('sheet/delete.all.html.twig',['schoolName'=>$school->getName()]);
        }
        else {
            throw $this->createNotFoundException(
                'No page found for id '.$survey->getId()
            );
        }
    }

    /**
     *@Route("survey/preview/{id}",name="survey_preview")
     */
    public function surveyPreview(Sheet $survey){
            //View of how the survey will look like before send it.

        return $this->render('sheet/preview.html.twig', ['survey'=> $survey]);
    }
 
/**
 * @Route("survey/anonymousId/{id}", name ="survey_anonymousId_list")
 */
public function getAnonymousIdListFromSurvey(Sheet $survey){
    //Display the list of anonymousId and password concerning a survey.

        return $this->render('sheet/anonymousId.list.html.twig',
                                ['anonymousIds'=> $survey->getAnonymousIds(), 'classes'=>$survey->getClasses(), 'survey'=>$survey]);
}


/**
 * @Route("survey/show/period", name="survey_show_period")
 */
    public function getSurveyListBetweenDate(Request $request, SheetRepository $surveyRepository){
       
    //Display a form to see a list of survey between 2 dates.

        //Create a form.
        $form = $this->createFormBuilder()
        ->add('start_date', DateType::class)
        ->add('end_date', DateType::class)
        ->add('submit', SubmitType::class)
        ->getForm();

        //Listen if the form was submittted.
        $form->handleRequest($request);

        //If the form was submitted and valid :
        if ($form->isSubmitted() && $form->isValid()) {
            
            //Collect data from the from.
            $start_date = $form->get('start_date')->getData();
            $end_date =$form->get('end_date')->getData();

            //Verifying if the start date is lower thant the end date.
            if($start_date < $end_date){
                
                //Search the surveys in the database created between these two dates.
                $surveys = $surveyRepository->findByDatePeriod($start_date,$end_date);

                //Display the list of surveys :
                return $this->render('sheet/survey.list.html.twig' ,['form'=>null, 'surveys'=>$surveys, 'message'=>null, 'date'=>array('start_date'=>$start_date,'end_date'=> $end_date) ]);
            }
            else{
                //Redictect to the form and send a error message that the start date must be lower than the end date.
                return $this->render('sheet/survey.list.html.twig' ,['form'=>$form->createView(), 'surveys'=>null , 'message' =>'Error the end date must be superior to the start date']);
            }
            
           ;

        }
            
            //Display the page with the form to select the period of the survey the user wants to be displayed.
            return $this->render('sheet/survey.list.html.twig', ['form' => $form->createView(), 'surveys'=>null, 'message'=>null]);
    }

    /**
     * @Route("/survey/show/one/{id}", name = "survey_show")
     */
    public function showSurvey(Sheet $survey){
            //Show the one survey and these infomation about it (creation date , update date, pages).

        return  $this->render('/sheet/survey.show.one.html.twig',['survey'=>$survey]);
    }
}
