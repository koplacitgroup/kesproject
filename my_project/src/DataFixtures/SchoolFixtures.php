<?php

namespace App\DataFixtures;

use App\Entity\School;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

//This class if used t create fake data for the database : create some school.
class SchoolFixtures extends Fixture
{

    
    public function load(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create('fr_FR');
        for ($i=1 ; $i <= 4 ; $i++) { 
        
        $school = new School();
        $city = $faker->city();
        $school->setName('college of '.$city)
               ->setAddress($city.' '.($faker->streetAddress()))
               ->setAcademy('academy of '.$city);
               
               $manager->persist($school);
              $this->addReference('school'.$i, $school);
        
        }
        $manager->flush();

        
    }
}
