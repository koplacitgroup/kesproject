<?php

namespace App\DataFixtures;

use App\Entity\Subject;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

/*This class if used to create fake data for the data base : 
  create some subjects.*/
class SubjectFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $subjectTab = array('Mathematika', 'Computer Science',
        'Biology', 'Chemistry', 'Czech', 'History', 'Geography',
        'Literature', 'French', 'Spanish', 'Philosophie', 'Law');
        $a =1;
        foreach ($subjectTab as $key => $sb) {
            
            $subject = new Subject();
            $subject->setName($sb);

        $manager->persist($subject);
        $manager->flush();

        $this->addReference('subject'.$a,$subject);
        $a++;

        }
       

        
    }

    public function getDependencies()
    {
        return array(
            StudentFixtures::class,
        );
    }
}
