<?php

namespace App\Repository;

use App\Entity\Response;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Response|null find($id, $lockMode = null, $lockVersion = null)
 * @method Response|null findOneBy(array $criteria, array $orderBy = null)
 * @method Response[]    findAll()
 * @method Response[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ResponseRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Response::class);
    }

    // /**
    //  * @return Response[] Returns an array of Response objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Response
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    public function countByQuestion($question)
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = 'SELECT  COUNT(response_id) as nbr FROM response Where question_id =:questionid';
        
        $stmt = $conn->prepare($sql);
    $stmt->execute(['questionid' => $question->getId()]);
        $result = $stmt->fetch();
    // returns an array of arrays (i.e. a raw data set)
    return intval($result['nbr']);
    }

    public function findOneMaxRatedResponseByQuestion($question){
        
        return $this->createQueryBuilder('r')
        ->select('r.id,Max(r.value) as max_value')
        ->where('r.question = ?1')
        ->setParameter(1, $question)
        ->getQuery()
        ->getOneOrNullResult()
        ;
    }
}


