<?php

namespace App\Repository;

use App\Entity\Websitecounter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Websitecounter|null find($id, $lockMode = null, $lockVersion = null)
 * @method Websitecounter|null findOneBy(array $criteria, array $orderBy = null)
 * @method Websitecounter[]    findAll()
 * @method Websitecounter[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WebsitecounterRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Websitecounter::class);
    }

    // /**
    //  * @return Websitecounter[] Returns an array of Websitecounter objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Websitecounter
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
