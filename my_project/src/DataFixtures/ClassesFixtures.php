<?php

namespace App\DataFixtures;

use App\Entity\Classes;
use App\DataFixtures\SchoolFixtures;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;


// This class is used to generate fake data for the database (create some classes for each school).
class ClassesFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
       $classExample = array( "10th", "11th" ,"12th");
        for ($j=1; $j<=4  ; $j++) { 
            foreach ($classExample as $key => $cl) {
                for ($i=1; $i <=4 ; $i++) { 
                    $classes = new Classes();
                    $classes->setName($cl.$i)
                            ->setYear(2019)
                            ->setSchool($this->getReference('school'.$j));
                            $manager->persist($classes);
                            $manager->flush();
                            $this->addReference($cl.$j.$i, $classes);
                 }
            }
        }
    }

    public function getDependencies(){
        return array(
            SchoolFixtures::class,
        );
    }
}
