<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\StudentRepository")
 */
class Student
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=100, unique=true)
     */
    private $email;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Classes", inversedBy="students")
     */
    private $classes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserValidSurvey", mappedBy="Student")
     */
    private $userValidSurveys;

    public function __construct()
    {
        $this->classes = new ArrayCollection();
        $this->userValidSurveys = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return Collection|classes[]
     */
    public function getClasses(): Collection
    {
        return $this->classes;
    }

    public function addClass(classes $class): self
    {
        if (!$this->classes->contains($class)) {
            $this->classes[] = $class;
        }

        return $this;
    }

    public function removeClass(classes $class): self
    {
        if ($this->classes->contains($class)) {
            $this->classes->removeElement($class);
        }

        return $this;
    }

    /**
     * @return Collection|UserValidSurvey[]
     */
    public function getUserValidSurveys(): Collection
    {
        return $this->userValidSurveys;
    }

    public function addUserValidSurvey(UserValidSurvey $userValidSurvey): self
    {
        if (!$this->userValidSurveys->contains($userValidSurvey)) {
            $this->userValidSurveys[] = $userValidSurvey;
            $userValidSurvey->setStudent($this);
        }

        return $this;
    }

    public function removeUserValidSurvey(UserValidSurvey $userValidSurvey): self
    {
        if ($this->userValidSurveys->contains($userValidSurvey)) {
            $this->userValidSurveys->removeElement($userValidSurvey);
            // set the owning side to null (unless already changed)
            if ($userValidSurvey->getStudent() === $this) {
                $userValidSurvey->setStudent(null);
            }
        }

        return $this;
    }
}
