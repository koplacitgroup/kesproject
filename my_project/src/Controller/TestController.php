<?php

namespace App\Controller;

use App\Entity\Sheet;
use App\Entity\Classes;
use App\Entity\Teacher;
use App\Entity\Question;
use App\Service\EmailService;
use App\Repository\TeacherRepository;
use App\Repository\ResponseRepository;
use App\Repository\UserResponseRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class TestController extends AbstractController
{
    /**
     * @Route("/test", name="test")
     */
    public function index(Request $request)
    { dump($request->getLocale() );
        return $this->render('test/index.html.twig', [
            'controller_name' => 'TestController',
        ]);
    }



    /**
     * @Route("/test/classes/{id}", name="test_classes")
     */
    public function index54(Classes $classes , TeacherRepository $repoteacher){
        $teachers = $repoteacher->findByOneClasses($classes);
        
        return $this->render('/test/teachers.html.twig',['teachers'=>$teachers]);
    }

     /**
     * @Route("/test/question/{id}", name="test_question")
     */
    public function index5(Question $question, ResponseRepository $reporesponse){
        $response = $reporesponse->findOneMaxRatedResponseByQuestion($question);
        dump($response);
        return $this->render('/test/question.html.twig');
    }

       /**
     * @Route("test/teacher/{id}/survey/{survey}", name="test_surv")
     */
    public function index50(Teacher $teacher,Sheet $survey,UserResponseRepository $userResponseRepository){
        $response = $userResponseRepository->findBySurveyAndTeacher($survey, $teacher);
        
        return $this->render('/test/hello.world.html.twig');
    }

     /**
     * @Route("test/request", name="test_request")
     */
    public function testRequest(Request $request){
    
        return $this->render('/test/hello.world.html.twig');
    }


    /**
     * @Route("test/email", name="test_email")
     */
    public function testEmail(Request $request, EmailService $emailService , \Swift_Mailer $mailer){
    
        
        $message = 'Teacher Information'."\n"
        .'-------------------------------------------------------------'."\n"
        . 'Teacher: {{teacher.firstname}} {{teacher.lastname}}.'."\n"
        . 'Subject: {% for tc in teacherClassesList %}{{tc.subject.name}}{% endfor %}'."\n"
        . 'Group: 5A'."\n"
         .'Classes: {% for tc in teacherClassesList %}{{tc.classes.name}}{% endfor %}'."\n"
        .'School: {{school.name}}'."\n"
        . 'E-mails: {{teacher.email}}'."\n"
        . '-------------------------------------------------------------'."\n"
        . 'Your mark: {{mark.mark}}'."\n"
        . 'Comments about you: {{commentnumber}}'."\n"
        . 'You have received feedback from: {{mark.numberOfResponses}} people'."\n"
        . '-------------------------------------------------------------'."\n";
        $email = "kevinpizeuil@gmail.com";
        $emailService->sendEmailtoTeacher($message,$email, $mailer);
        return $this->render('/test/hello.world.html.twig');
    }


   

}
