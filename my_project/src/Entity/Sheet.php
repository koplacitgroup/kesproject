<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SheetRepository")
 */
class Sheet
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Classes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $classes;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Page", mappedBy="sheet", orphanRemoval=true)
     */
    private $pages;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserValidSurvey", mappedBy="Sheet")
     */
    private $userValidSurveys;

    /**
     * @ORM\Column(type="boolean")
     */
    private $sendable;

    /**
     * @ORM\Column(type="boolean")
     */
    private $sended;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\AnonymousId", mappedBy="survey")
     */
    private $anonymousIds;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\SurveyGroup", mappedBy="surveys")
     */
    private $surveyGroups;

 
    public function __construct()
    {
        $this->pages = new ArrayCollection();
        $this->userValidSurveys = new ArrayCollection();
    
        $this->anonymousIdList = new ArrayCollection();
        $this->teacherSurveyMarks = new ArrayCollection();
        $this->anonymousIds = new ArrayCollection();
        $this->surveyGroups = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getClasses(): ?classes
    {
        return $this->classes;
    }

    public function setClasses(?classes $classes): self
    {
        $this->classes = $classes;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return Collection|Page[]
     */
    public function getPages(): Collection
    {
        return $this->pages;
    }

    public function addPage(Page $page): self
    {
        if (!$this->pages->contains($page)) {
            $this->pages[] = $page;
            $page->setSheet($this);
        }

        return $this;
    }

    public function removePage(Page $page): self
    {
        if ($this->pages->contains($page)) {
            $this->pages->removeElement($page);
            // set the owning side to null (unless already changed)
            if ($page->getSheet() === $this) {
                $page->setSheet(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserValidSurvey[]
     */
    public function getUserValidSurveys(): Collection
    {
        return $this->userValidSurveys;
    }

    public function addUserValidSurvey(UserValidSurvey $userValidSurvey): self
    {
        if (!$this->userValidSurveys->contains($userValidSurvey)) {
            $this->userValidSurveys[] = $userValidSurvey;
            $userValidSurvey->setSheet($this);
        }

        return $this;
    }

    public function removeUserValidSurvey(UserValidSurvey $userValidSurvey): self
    {
        if ($this->userValidSurveys->contains($userValidSurvey)) {
            $this->userValidSurveys->removeElement($userValidSurvey);
            // set the owning side to null (unless already changed)
            if ($userValidSurvey->getSheet() === $this) {
                $userValidSurvey->setSheet(null);
            }
        }

        return $this;
    }

    public function getSendable(): ?bool
    {
        return $this->sendable;
    }

    public function setSendable(bool $sendable): self
    {
        $this->sendable = $sendable;

        return $this;
    }

    public function getSended(): ?bool
    {
        return $this->sended;
    }

    public function setSended(bool $sended): self
    {
        $this->sended = $sended;

        return $this;
    }

    /**
     * @return Collection|AnonymousId[]
     */
    public function getAnonymousIds(): Collection
    {
        return $this->anonymousIds;
    }

    public function addAnonymousId(AnonymousId $anonymousId): self
    {
        if (!$this->anonymousIds->contains($anonymousId)) {
            $this->anonymousIds[] = $anonymousId;
            $anonymousId->setSurvey($this);}
        }

    public function removeAnonymousId(AnonymousId $anonymousId): self
    {
        if ($this->anonymousIds->contains($anonymousId)) {
            $this->anonymousIds->removeElement($anonymousId);
            // set the owning side to null (unless already changed)
            if ($anonymousId->getSurvey() === $this) {
                $anonymousId->setSurvey(null);

            }
        }
        }

    /**
     * @return Collection|SurveyGroup[]
     */
    public function getSurveyGroups(): Collection
    {
        return $this->surveyGroups;
    }

    public function addSurveyGroup(SurveyGroup $surveyGroup): self
    {
        if (!$this->surveyGroups->contains($surveyGroup)) {
            $this->surveyGroups[] = $surveyGroup;
            $surveyGroup->addSurvey($this);
        }

        return $this;
    }

    public function removeSurveyGroup(SurveyGroup $surveyGroup): self
    {
        if ($this->surveyGroups->contains($surveyGroup)) {
            $this->surveyGroups->removeElement($surveyGroup);
            $surveyGroup->removeSurvey($this);
        }

        return $this;
    }
    
}