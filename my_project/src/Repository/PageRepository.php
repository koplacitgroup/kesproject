<?php

namespace App\Repository;

use App\Entity\Page;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Page|null find($id, $lockMode = null, $lockVersion = null)
 * @method Page|null findOneBy(array $criteria, array $orderBy = null)
 * @method Page[]    findAll()
 * @method Page[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PageRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Page::class);
    }

    // /**
    //  * @return Page[] Returns an array of Page objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Page
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findOneByQuestion($question)
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = 'SELECT page_id from page_question pq Where pq.question_id = :questionId';
       
        $stmt = $conn->prepare($sql);
        $stmt->execute(['questionId' => $question->getId()]);

        // returns an array of arrays (i.e. a raw data set)
        return $stmt->fetch();
    }


    public function removeOneQuestion($page, $question){
        $conn = $this->getEntityManager()->getConnection();
        $sql = 'DELETE  FROM page_question WHERE question_id = :questionId AND page_id = :pageId';
       
        $stmt = $conn->prepare($sql);
       return $stmt->execute(['questionId' => $question->getId(),'pageId' => $page->getId()]);

    }

    public function removeAllQuestion($page){
        $conn = $this->getEntityManager()->getConnection();
        $sql = 'DELETE  FROM page_question WHERE  page_id = :pageId';
       
        $stmt = $conn->prepare($sql);
       return $stmt->execute(['pageId' => $page->getId()]);

    }

    public function findByTeacherAndSurvey($survey,$teacher){
        return $this->createQueryBuilder('p')
            ->Where('p.teacher = :teacher')
            ->andWhere('p.sheet = :survey')
            ->setParameters(array('teacher'=>$teacher,'survey'=>$survey))
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
}