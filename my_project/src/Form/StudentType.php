<?php

namespace App\Form;

use App\Entity\User;
use App\Entity\School;
use App\Entity\Classes;
use App\Entity\UserStudent;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

//Form to register a new student.
class StudentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            
        ->add('lastname')
        ->add('firstname')
        ->add('email')
        ->add('gender', ChoiceType::class, [
            'choices'  => [
                'Male' => 'male',
                'Female' =>'female'
            ],
            'expanded' => true,
            'multiple' =>false
        ])
        ->add('password',PasswordType::class)
        ->add('confirm_password', PasswordType::class)
    
        ;

      
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
