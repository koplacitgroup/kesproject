-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  mer. 07 août 2019 à 15:03
-- Version du serveur :  10.3.16-MariaDB
-- Version de PHP :  7.2.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `teachers_evaluation`
--

-- --------------------------------------------------------

--
-- Structure de la table `anonymous_id`
--

CREATE TABLE `anonymous_id` (
  `id` int(11) NOT NULL,
  `survey_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `valid` tinyint(1) NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `anonymous_id`
--

INSERT INTO `anonymous_id` (`id`, `survey_id`, `created_at`, `valid`, `password`, `link`) VALUES
(4, 5, '2019-07-25 13:20:29', 0, 'testtest', NULL),
(5, 5, '2019-07-25 13:20:29', 0, 'testtest', NULL),
(6, 5, '2019-07-25 13:20:29', 0, 'testtest', NULL),
(7, 6, '2019-07-25 13:44:02', 0, 'testtest', NULL),
(8, 6, '2019-07-25 13:44:02', 0, 'testtest', NULL),
(9, 6, '2019-07-25 13:44:02', 1, 'testtest', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `anonymous_id_page`
--

CREATE TABLE `anonymous_id_page` (
  `anonymous_id_id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `anonymous_id_page`
--

INSERT INTO `anonymous_id_page` (`anonymous_id_id`, `page_id`) VALUES
(4, 12),
(4, 14),
(5, 13),
(5, 14),
(6, 12),
(6, 13),
(6, 14),
(7, 15),
(7, 16),
(7, 17),
(8, 15),
(8, 16),
(8, 17),
(9, 15),
(9, 17);

-- --------------------------------------------------------

--
-- Structure de la table `classes`
--

CREATE TABLE `classes` (
  `id` int(11) NOT NULL,
  `school_id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `year` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `classes`
--

INSERT INTO `classes` (`id`, `school_id`, `name`, `year`) VALUES
(1, 1, '1A', 2019),
(2, 1, '1B', 2019),
(5, 3, '1.A', 2020),
(6, 3, '1.B', 2020),
(7, 3, '1.C', 2020),
(8, 3, '1.D', 2020),
(9, 3, '1.E', 2020),
(10, 3, '2.A', 2020),
(11, 3, '2.B', 2020),
(12, 3, '2.C', 2020),
(13, 3, '2.D', 2020),
(14, 3, '3.A', 2020),
(15, 3, '3.D', 2020),
(16, 3, '4.A', 2020),
(17, 3, '4.B', 2020),
(18, 3, '4.C', 2020),
(19, 3, '4.D', 2020),
(20, 3, '4.E', 2020),
(21, 3, '5.A', 2020),
(22, 3, '6.A', 2020),
(23, 4, '3.B', 2020),
(24, 4, '3.C', 2020),
(25, 4, '1ag', 2020),
(26, 4, '1bg', 2020),
(27, 4, '2ag', 2020),
(28, 4, '2bg', 2020),
(29, 4, '3ag', 2020),
(30, 4, '3bg', 2020),
(31, 4, '4ag', 2020),
(32, 4, '4bg', 2020),
(33, 4, '1.A', 2020),
(34, 4, '1.B', 2020),
(35, 4, '1.C', 2020),
(36, 4, '1.D', 2020),
(37, 4, '2.A', 2020),
(38, 4, '2.B', 2020),
(39, 4, '2.D', 2020),
(40, 4, '3.A', 2020),
(41, 4, '3.D', 2020),
(42, 4, '4.A', 2020),
(43, 4, '4.B', 2020),
(44, 4, '4.C', 2020),
(45, 4, '4.D', 2020);

-- --------------------------------------------------------

--
-- Structure de la table `comment`
--

CREATE TABLE `comment` (
  `id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `creation_date` date NOT NULL,
  `text` longtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `comment`
--

INSERT INTO `comment` (`id`, `page_id`, `creation_date`, `text`) VALUES
(8, 12, '2019-07-25', 'Some text about teacher'),
(9, 14, '2019-07-25', 'Some text about teacher'),
(10, 13, '2019-07-25', 'Some text about teacher'),
(11, 14, '2019-07-25', 'Some text about teacher'),
(12, 12, '2019-07-25', 'Some text about teacher'),
(13, 13, '2019-07-25', 'Some text about teacher'),
(14, 15, '2019-07-25', 'Some text about teacher'),
(15, 16, '2019-07-25', 'Some text about teacher'),
(16, 17, '2019-07-25', 'Some text about teacher'),
(17, 15, '2019-07-25', 'Some text about teacher'),
(18, 16, '2019-07-25', 'Some text about teacher'),
(19, 17, '2019-07-25', 'Some text about teacher');

-- --------------------------------------------------------

--
-- Structure de la table `migration_versions`
--

CREATE TABLE `migration_versions` (
  `version` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `executed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `migration_versions`
--

INSERT INTO `migration_versions` (`version`, `executed_at`) VALUES
('20190722080008', '2019-07-22 08:02:04'),
('20190725091338', '2019-07-25 09:13:59'),
('20190725094112', '2019-07-25 09:41:21'),
('20190729131004', '2019-07-29 13:10:32'),
('20190729133938', '2019-07-29 13:39:52'),
('20190729145321', '2019-07-29 14:53:38'),
('20190730081850', '2019-07-30 08:18:59'),
('20190802120856', '2019-08-02 12:09:11');

-- --------------------------------------------------------

--
-- Structure de la table `page`
--

CREATE TABLE `page` (
  `id` int(11) NOT NULL,
  `sheet_id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `page`
--

INSERT INTO `page` (`id`, `sheet_id`, `teacher_id`, `subject_id`) VALUES
(12, 5, 1, 1),
(13, 5, 2, 2),
(14, 5, 5, 2),
(15, 6, 3, 3),
(16, 6, 4, 4),
(17, 6, 6, 4);

-- --------------------------------------------------------

--
-- Structure de la table `page_question`
--

CREATE TABLE `page_question` (
  `page_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `page_question`
--

INSERT INTO `page_question` (`page_id`, `question_id`) VALUES
(12, 1),
(12, 2),
(13, 1),
(13, 2),
(14, 1),
(14, 2),
(15, 1),
(15, 2),
(16, 1),
(16, 2),
(17, 1),
(17, 2);

-- --------------------------------------------------------

--
-- Structure de la table `question`
--

CREATE TABLE `question` (
  `id` int(11) NOT NULL,
  `text` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `question`
--

INSERT INTO `question` (`id`, `text`, `type`) VALUES
(1, 'How was the course ?', 'unique'),
(2, 'How was the quality of the documents provided by the teacher ?', 'unique');

-- --------------------------------------------------------

--
-- Structure de la table `question_standard`
--

CREATE TABLE `question_standard` (
  `id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `question_standard`
--

INSERT INTO `question_standard` (`id`, `question_id`) VALUES
(1, 1),
(2, 2);

-- --------------------------------------------------------

--
-- Structure de la table `response`
--

CREATE TABLE `response` (
  `id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `text` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `response`
--

INSERT INTO `response` (`id`, `question_id`, `text`, `value`) VALUES
(1, 1, 'Unsatisfying', 0),
(2, 2, 'Unsatifying', 0),
(3, 1, 'Satisfying', 1),
(4, 2, 'Satisfying', 1),
(5, 1, 'Good', 2),
(6, 2, 'Good', 2),
(7, 1, 'Very Good', 3),
(8, 2, 'Very Good', 3);

-- --------------------------------------------------------

--
-- Structure de la table `response_standard`
--

CREATE TABLE `response_standard` (
  `id` int(11) NOT NULL,
  `text` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `school`
--

CREATE TABLE `school` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `academy` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `school`
--

INSERT INTO `school` (`id`, `name`, `address`, `academy`) VALUES
(1, 'School of Cz', 'cz cz', 'Czech'),
(2, 'School of Slk', 'slk slk', 'Slovakia'),
(3, 'Krenoba High School', 'Krenova High School Brno', 'Czech Republic Brno'),
(4, 'Jaroska Seznam', 'Jaroska Seznam Brno', 'Czech Republic Brno');

-- --------------------------------------------------------

--
-- Structure de la table `school_subject`
--

CREATE TABLE `school_subject` (
  `id` int(11) NOT NULL,
  `school_id` int(11) NOT NULL,
  `subject_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `school_subject`
--

INSERT INTO `school_subject` (`id`, `school_id`, `subject_name`, `subject_code`) VALUES
(417, 1, 'A', 'A'),
(418, 1, 'Bi', 'Bi'),
(419, 1, '?', '?'),
(420, 1, 'D', 'D'),
(421, 1, 'F', 'F'),
(422, 1, 'Fr', 'Fr'),
(423, 1, 'Hv', 'Hv'),
(424, 1, 'Ch', 'Ch'),
(425, 1, 'In', 'In'),
(426, 1, 'M', 'M'),
(427, 1, 'Mcv', 'Mcv'),
(428, 1, 'N', 'N'),
(429, 1, 'R', 'R'),
(430, 1, '?', '?'),
(431, 1, 'Tvd', 'Tvd'),
(432, 1, 'Tvh', 'Tvh'),
(433, 1, 'Vv', 'Vv'),
(434, 1, 'Z', 'Z'),
(435, 1, 'Sv', 'Sv'),
(436, 1, 'CvMn', 'CvMn'),
(437, 1, '?cv', '?cv'),
(438, 1, 'CvMF', 'CvMF'),
(439, 1, 'Du3L', 'Du3L'),
(440, 1, 'In3L', 'In3L'),
(441, 1, 'La3L', 'La3L'),
(442, 1, 'Mol', 'Mol'),
(443, 1, 'Rv', 'Rv'),
(444, 1, 'Ak2L', 'Ak2L'),
(445, 1, 'CvBiCh', 'CvBiCh'),
(446, 1, 'Dg', 'Dg'),
(447, 1, 'Ek', 'Ek'),
(448, 1, 'Inp', 'Inp'),
(449, 1, 'La2L', 'La2L'),
(450, 1, 'Du2L', 'Du2L'),
(451, 1, 'Fin', 'Fin'),
(452, 1, 'Rk1L', 'Rk1L'),
(453, 1, 'Sbi', 'Sbi'),
(454, 1, 'SF', 'SF'),
(455, 1, 'SCh', 'SCh'),
(456, 1, 'Sin', 'Sin'),
(457, 1, 'SM', 'SM'),
(458, 1, 'SMA', 'SMA'),
(459, 1, 'Tv', 'Tv'),
(460, 1, 'Ak1L', 'Ak1L'),
(461, 1, 'Ar', 'Ar'),
(462, 1, 'S?', 'S?'),
(463, 1, 'Sek', 'Sek'),
(464, 1, 'SPS', 'SPS'),
(465, 1, 'SSv', 'SSv'),
(466, 1, 'Ov', 'Ov'),
(467, 1, 'Inn', 'Inn'),
(468, 1, 'Nn', 'Nn'),
(469, 1, '?n', '?n');

-- --------------------------------------------------------

--
-- Structure de la table `sheet`
--

CREATE TABLE `sheet` (
  `id` int(11) NOT NULL,
  `classes_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `sendable` tinyint(1) NOT NULL,
  `sended` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `sheet`
--

INSERT INTO `sheet` (`id`, `classes_id`, `title`, `created_at`, `updated_at`, `sendable`, `sended`) VALUES
(5, 1, 'This is the title of the survey', '2019-07-25 13:20:24', NULL, 1, 1),
(6, 2, 'This is the title of the survey 1b', '2019-07-25 13:43:57', NULL, 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `student`
--

CREATE TABLE `student` (
  `id` int(11) NOT NULL,
  `lastname` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `firstname` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `student`
--

INSERT INTO `student` (`id`, `lastname`, `firstname`, `email`) VALUES
(1, 'Jetion', 'Jeremiah', 'jjeremiah@mail.com'),
(2, 'Bastian', 'Sophia', 'bsophia@mail.com'),
(3, 'Martin', 'Kenan', 'mkenan@mail.com'),
(4, 'Girard', 'Samantha', 'gsamantha@mail.com'),
(5, 'Nelson', 'Gregory', 'ngregory@mail.com'),
(6, 'Netstra', 'Estella', 'nestella@mail.com');

-- --------------------------------------------------------

--
-- Structure de la table `student_classes`
--

CREATE TABLE `student_classes` (
  `student_id` int(11) NOT NULL,
  `classes_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `student_classes`
--

INSERT INTO `student_classes` (`student_id`, `classes_id`) VALUES
(1, 1),
(2, 1),
(3, 2),
(4, 2),
(5, 1),
(6, 2);

-- --------------------------------------------------------

--
-- Structure de la table `subject`
--

CREATE TABLE `subject` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `subject`
--

INSERT INTO `subject` (`id`, `name`) VALUES
(1, 'math'),
(2, 'history'),
(3, 'czech'),
(4, 'physics'),
(5, 'biology');

-- --------------------------------------------------------

--
-- Structure de la table `survey_in_progress`
--

CREATE TABLE `survey_in_progress` (
  `id` int(11) NOT NULL,
  `survey_id` int(11) NOT NULL,
  `sended_at` datetime NOT NULL,
  `finished_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `survey_in_progress`
--

INSERT INTO `survey_in_progress` (`id`, `survey_id`, `sended_at`, `finished_at`) VALUES
(4, 5, '2019-07-25 13:20:29', NULL),
(5, 6, '2019-07-25 13:44:02', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `teacher`
--

CREATE TABLE `teacher` (
  `id` int(11) NOT NULL,
  `lastname` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `firstname` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `teacher_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `teacher`
--

INSERT INTO `teacher` (`id`, `lastname`, `firstname`, `email`, `teacher_code`) VALUES
(1, 'johnathan', 'marcus', 'jmarcus@mail.com', 'jhm'),
(2, 'Sanders', 'Mina', 'smina@mail.com', 'smi'),
(3, 'Anime', 'Carla', 'acarla@mail.com', 'aca'),
(4, 'Homton', 'Quentin', 'hquentin@mail.com', 'hqu'),
(5, 'Danston', 'Oceanna', 'doceanna@mail.com', 'doc'),
(6, 'Wisdor', 'Wilson', 'wwilson@mail.com', 'wwi'),
(8, 'Nicolas', 'James', 'nicolas@mail.com', 'nja'),
(9, 'Thierry', 'Marc', 'thierrry@mail.com', 'tma'),
(10, 'Nicoa', 'Melissa', 'melissa@mail.com', 'nme');

-- --------------------------------------------------------

--
-- Structure de la table `teacher_classes`
--

CREATE TABLE `teacher_classes` (
  `id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `classes_id` int(11) NOT NULL,
  `school_subject_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `teacher_classes`
--

INSERT INTO `teacher_classes` (`id`, `teacher_id`, `subject_id`, `classes_id`, `school_subject_id`) VALUES
(1, 1, 1, 1, NULL),
(2, 2, 2, 1, NULL),
(3, 3, 3, 2, NULL),
(4, 4, 4, 2, NULL),
(5, 5, 2, 1, NULL),
(6, 6, 4, 2, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `teacher_page_mark`
--

CREATE TABLE `teacher_page_mark` (
  `id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `mark` double NOT NULL,
  `number_of_responses` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `teacher_page_mark`
--

INSERT INTO `teacher_page_mark` (`id`, `teacher_id`, `page_id`, `mark`, `number_of_responses`) VALUES
(4, 1, 12, 2.915, 2),
(5, 5, 14, 2.5, 3),
(6, 2, 13, 4.165, 2),
(7, 3, 15, 4.17, 2),
(8, 4, 16, 2.92, 2),
(9, 6, 17, 2.92, 2);

-- --------------------------------------------------------

--
-- Structure de la table `teacher_subject`
--

CREATE TABLE `teacher_subject` (
  `teacher_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `teacher_subject`
--

INSERT INTO `teacher_subject` (`teacher_id`, `subject_id`) VALUES
(5, 2),
(5, 3),
(6, 1),
(6, 4);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `school_id` int(11) DEFAULT NULL,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `firstname` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(6) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `school_id`, `email`, `roles`, `password`, `lastname`, `firstname`, `gender`) VALUES
(1, 1, 'admin@mail.com', '[\"ROLE_ADMIN\"]', '$2y$13$HCN8RElwG8XYlWlsFG9gIu0L6QLOa9an2Lk25h2sHpJAEZF9PeuJS', 'admin', 'admin', 'male'),
(2, 1, 'ngregory@mail.com', '[\"ROLE_STUDENT\"]', '$2y$13$grx2uI21jmldrQdwNzWzBezapmTIUvV7DJ7ZniTw8Sk/KAmLjFh0.', 'Nelson', 'Gregory', 'male'),
(3, 1, 'nestella@mail.com', '[\"ROLE_STUDENT\"]', '$2y$13$353RuCvXlsleac3eCEh2n.3MldV7oB/W/GZWGEW6rKg5e4aqIltPu', 'Netstra', 'Estella', 'female'),
(4, 1, 'doceanna@mail.com', '[\"ROLE_TEACHER\"]', '$2y$13$5zh/Icy47QSOQG8cdKYW6ue1FwPKUruyvv0LqWWs38IEBK4UHMPVe', 'Danston', 'Oceanna', 'female'),
(5, 1, 'wwilson@mail.com', '[\"ROLE_TEACHER\"]', '$2y$13$XPNpbIMrJCVGaW06FFdeq.Ozv3.ePt5ENj5qQWoJdWv7aXBfuJdU2', 'Wisdor', 'Wilson', 'male'),
(6, 1, 'jmarcus@mail.com', '[\"ROLE_TEACHER\"]', '$2y$13$e9Dfy5050ppPoZfgIds4Ee1/RcIF..qEWu0yRtwoWNFSCmKVr/5by', 'marcus', 'johnathan', 'male'),
(7, 1, 'smina@mail.com', '[\"ROLE_TEACHER\"]', '$2y$13$NUHusYifMmk6V6JjwuEwbu0G6GIUTJQv9EXFIrX5JVXiBVbb32eKW', 'Sanders', 'Mina', 'female'),
(8, 1, 'acarla@mail.com', '[\"ROLE_TEACHER\"]', '$2y$13$MhZe0y7.s9mtnZ6zr6X1G.qBUF94P5K68D35co6S22E.tacyJTf3i', 'Anime', 'Carla', 'female'),
(9, 1, 'hquentin@mail.com', '[\"ROLE_TEACHER\"]', '$2y$13$5AJ8tggFnpuvSWRL83K9VuPwcKIS/k80W0wJvhZ8BBm8HKBrFHx4m', 'Homton', 'Quentin', 'male'),
(11, 1, 'jjeremiah@mail.com', '[\"ROLE_STUDENT\"]', '$2y$13$mTIKt87ml5Fys9/vhoUQLeksd/qpvncEwW0LbxrmvD2Y67xmQbbPG', 'Jeremiah', 'Jetion', 'male'),
(12, 1, 'bsophia@mail.com', '[\"ROLE_STUDENT\"]', '$2y$13$uRaJvMKgfvxakPmr8eeDRuJvDhwZPXLvtMQ.6uvt02NDsjnGdaD3.', 'Sophia', 'Bastian', 'female'),
(13, 1, 'mkenan@mail.com', '[\"ROLE_STUDENT\"]', '$2y$13$PBNAN6q.EbNB9EzJaPhXdOdDk3wyaYPVkFZMdMnqKLOswZK5FieZW', 'Kenan', 'Martin', 'male'),
(14, 1, 'gsamantha@mail.com', '[\"ROLE_STUDENT\"]', '$2y$13$AdliXyLBBkTkLuSwV4OMnebEjdcx6E.uycfB7z6TLIPvzCRyTq9k2', 'Samantha', 'Girard', 'female'),
(15, 1, 'superadmin@mail.com', '[\"ROLE_SUPER_ADMIN\"]', '$2y$13$9wKmYx1LBNON6ikFAY/LNuiXdqfSK.FVPvZt/wDpYHVE.bVYA.m8e', 'superadmin', 'superadmin', 'male'),
(16, 1, 'nicolas@mail.com', '[\"ROLE_TEACHER\"]', '$2y$13$JwPQIRngm9iarfQ7CFVnweKgH7/MFP6H5Zp9n2xQJHP3TCfdrf72i', 'James', 'Nicolas', 'male'),
(17, 1, 'thierrry@mail.com', '[\"ROLE_TEACHER\"]', '$2y$13$Rp8yYQtwp8SoGp3bfptbIeS1F0fr99P8GIGX3RlN/L8ewZ5URXOGe', 'Marc', 'Thierry', 'male'),
(18, 1, 'melissa@mail.com', '[\"ROLE_TEACHER\"]', '$2y$13$fzORt0qyKRtvhS3G2hdXV.9IH2itPeGJInq1U80OX7WKhm.KfPP5O', 'Melissa', 'Nicoa', 'female');

-- --------------------------------------------------------

--
-- Structure de la table `user_page`
--

CREATE TABLE `user_page` (
  `id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `anonymous_id_id` int(11) NOT NULL,
  `valid` tinyint(1) NOT NULL,
  `mark` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user_page`
--

INSERT INTO `user_page` (`id`, `page_id`, `anonymous_id_id`, `valid`, `mark`) VALUES
(6, 12, 4, 1, 2.5),
(7, 14, 4, 1, 5),
(8, 13, 5, 1, 3.33),
(9, 14, 5, 1, 1.67),
(10, 12, 6, 1, 3.33),
(11, 13, 6, 1, 5),
(12, 14, 6, 1, 0.83),
(13, 15, 7, 1, 4.17),
(14, 16, 7, 1, 4.17),
(15, 17, 7, 1, 2.5),
(16, 15, 8, 1, 4.17),
(17, 16, 8, 1, 1.67),
(18, 17, 8, 1, 3.33);

-- --------------------------------------------------------

--
-- Structure de la table `user_response`
--

CREATE TABLE `user_response` (
  `id` int(11) NOT NULL,
  `anonymous_id_id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `response_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user_response`
--

INSERT INTO `user_response` (`id`, `anonymous_id_id`, `page_id`, `response_id`) VALUES
(15, 4, 12, 3),
(16, 4, 12, 6),
(17, 4, 14, 7),
(18, 4, 14, 8),
(19, 5, 13, 3),
(20, 5, 13, 8),
(21, 5, 14, 5),
(22, 5, 14, 2),
(23, 6, 12, 3),
(24, 6, 12, 8),
(25, 6, 13, 7),
(26, 6, 13, 8),
(27, 6, 14, 1),
(28, 6, 14, 4),
(29, 7, 15, 5),
(30, 7, 15, 8),
(31, 7, 16, 5),
(32, 7, 16, 8),
(33, 7, 17, 3),
(34, 7, 17, 6),
(35, 8, 15, 5),
(36, 8, 15, 8),
(37, 8, 16, 1),
(38, 8, 16, 6),
(39, 8, 17, 7),
(40, 8, 17, 4);

-- --------------------------------------------------------

--
-- Structure de la table `user_sheet`
--

CREATE TABLE `user_sheet` (
  `id` int(11) NOT NULL,
  `anonymous_id_id` int(11) NOT NULL,
  `sheet_id` int(11) NOT NULL,
  `valid` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user_sheet`
--

INSERT INTO `user_sheet` (`id`, `anonymous_id_id`, `sheet_id`, `valid`) VALUES
(2, 4, 5, 1),
(3, 5, 5, 1),
(4, 6, 5, 1),
(5, 7, 6, 1),
(6, 8, 6, 1);

-- --------------------------------------------------------

--
-- Structure de la table `user_student`
--

CREATE TABLE `user_student` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user_student`
--

INSERT INTO `user_student` (`id`, `student_id`, `user_id`) VALUES
(1, 5, 2),
(2, 6, 3),
(3, 1, 11),
(4, 2, 12),
(5, 3, 13),
(6, 4, 14);

-- --------------------------------------------------------

--
-- Structure de la table `user_teacher`
--

CREATE TABLE `user_teacher` (
  `id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user_teacher`
--

INSERT INTO `user_teacher` (`id`, `teacher_id`, `user_id`) VALUES
(1, 5, 4),
(2, 6, 5),
(3, 1, 6),
(4, 2, 7),
(5, 3, 8),
(6, 4, 9),
(8, 8, 16),
(9, 9, 17),
(10, 10, 18);

-- --------------------------------------------------------

--
-- Structure de la table `user_valid_survey`
--

CREATE TABLE `user_valid_survey` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `sheet_id` int(11) NOT NULL,
  `validate` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `websitecounter`
--

CREATE TABLE `websitecounter` (
  `id` int(11) NOT NULL,
  `count` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `websitecounter`
--

INSERT INTO `websitecounter` (`id`, `count`) VALUES
(1, 3);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `anonymous_id`
--
ALTER TABLE `anonymous_id`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_FA93803B3FE509D` (`survey_id`);

--
-- Index pour la table `anonymous_id_page`
--
ALTER TABLE `anonymous_id_page`
  ADD PRIMARY KEY (`anonymous_id_id`,`page_id`),
  ADD KEY `IDX_6A211685DC92DCD1` (`anonymous_id_id`),
  ADD KEY `IDX_6A211685C4663E4` (`page_id`);

--
-- Index pour la table `classes`
--
ALTER TABLE `classes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_2ED7EC5C32A47EE` (`school_id`);

--
-- Index pour la table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_9474526CC4663E4` (`page_id`);

--
-- Index pour la table `migration_versions`
--
ALTER TABLE `migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Index pour la table `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_140AB6208B1206A5` (`sheet_id`),
  ADD KEY `IDX_140AB62041807E1D` (`teacher_id`),
  ADD KEY `IDX_140AB62023EDC87` (`subject_id`);

--
-- Index pour la table `page_question`
--
ALTER TABLE `page_question`
  ADD PRIMARY KEY (`page_id`,`question_id`),
  ADD KEY `IDX_36684E6EC4663E4` (`page_id`),
  ADD KEY `IDX_36684E6E1E27F6BF` (`question_id`);

--
-- Index pour la table `question`
--
ALTER TABLE `question`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `question_standard`
--
ALTER TABLE `question_standard`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_73FF678B1E27F6BF` (`question_id`);

--
-- Index pour la table `response`
--
ALTER TABLE `response`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_3E7B0BFB1E27F6BF` (`question_id`);

--
-- Index pour la table `response_standard`
--
ALTER TABLE `response_standard`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `school`
--
ALTER TABLE `school`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `school_subject`
--
ALTER TABLE `school_subject`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_69E10CE2C32A47EE` (`school_id`);

--
-- Index pour la table `sheet`
--
ALTER TABLE `sheet`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_873C91E29E225B24` (`classes_id`);

--
-- Index pour la table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_B723AF33E7927C74` (`email`);

--
-- Index pour la table `student_classes`
--
ALTER TABLE `student_classes`
  ADD PRIMARY KEY (`student_id`,`classes_id`),
  ADD KEY `IDX_EFDBCB3DCB944F1A` (`student_id`),
  ADD KEY `IDX_EFDBCB3D9E225B24` (`classes_id`);

--
-- Index pour la table `subject`
--
ALTER TABLE `subject`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `survey_in_progress`
--
ALTER TABLE `survey_in_progress`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_DD00511FB3FE509D` (`survey_id`);

--
-- Index pour la table `teacher`
--
ALTER TABLE `teacher`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_B0F6A6D5E7927C74` (`email`);

--
-- Index pour la table `teacher_classes`
--
ALTER TABLE `teacher_classes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_CF2FF38441807E1D` (`teacher_id`),
  ADD KEY `IDX_CF2FF38423EDC87` (`subject_id`),
  ADD KEY `IDX_CF2FF3849E225B24` (`classes_id`),
  ADD KEY `IDX_CF2FF384B79F5C75` (`school_subject_id`);

--
-- Index pour la table `teacher_page_mark`
--
ALTER TABLE `teacher_page_mark`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_9BDBA2B041807E1D` (`teacher_id`),
  ADD KEY `IDX_9BDBA2B0C4663E4` (`page_id`);

--
-- Index pour la table `teacher_subject`
--
ALTER TABLE `teacher_subject`
  ADD PRIMARY KEY (`teacher_id`,`subject_id`),
  ADD KEY `IDX_360CB33B41807E1D` (`teacher_id`),
  ADD KEY `IDX_360CB33B23EDC87` (`subject_id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`),
  ADD KEY `IDX_8D93D649C32A47EE` (`school_id`);

--
-- Index pour la table `user_page`
--
ALTER TABLE `user_page`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_6E8BFAE9C4663E4` (`page_id`),
  ADD KEY `IDX_6E8BFAE9DC92DCD1` (`anonymous_id_id`);

--
-- Index pour la table `user_response`
--
ALTER TABLE `user_response`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_DEF6EFFBDC92DCD1` (`anonymous_id_id`),
  ADD KEY `IDX_DEF6EFFBC4663E4` (`page_id`),
  ADD KEY `IDX_DEF6EFFBFBF32840` (`response_id`);

--
-- Index pour la table `user_sheet`
--
ALTER TABLE `user_sheet`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_65FE6ABADC92DCD1` (`anonymous_id_id`),
  ADD KEY `IDX_65FE6ABA8B1206A5` (`sheet_id`);

--
-- Index pour la table `user_student`
--
ALTER TABLE `user_student`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_EF2EB139CB944F1A` (`student_id`),
  ADD UNIQUE KEY `UNIQ_EF2EB139A76ED395` (`user_id`);

--
-- Index pour la table `user_teacher`
--
ALTER TABLE `user_teacher`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_E8FBB8DF41807E1D` (`teacher_id`),
  ADD UNIQUE KEY `UNIQ_E8FBB8DFA76ED395` (`user_id`);

--
-- Index pour la table `user_valid_survey`
--
ALTER TABLE `user_valid_survey`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_78E09C0CB944F1A` (`student_id`),
  ADD KEY `IDX_78E09C08B1206A5` (`sheet_id`);

--
-- Index pour la table `websitecounter`
--
ALTER TABLE `websitecounter`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `anonymous_id`
--
ALTER TABLE `anonymous_id`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT pour la table `classes`
--
ALTER TABLE `classes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT pour la table `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT pour la table `page`
--
ALTER TABLE `page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT pour la table `question`
--
ALTER TABLE `question`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `question_standard`
--
ALTER TABLE `question_standard`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `response`
--
ALTER TABLE `response`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `response_standard`
--
ALTER TABLE `response_standard`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `school`
--
ALTER TABLE `school`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `school_subject`
--
ALTER TABLE `school_subject`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=470;

--
-- AUTO_INCREMENT pour la table `sheet`
--
ALTER TABLE `sheet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `student`
--
ALTER TABLE `student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `subject`
--
ALTER TABLE `subject`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `survey_in_progress`
--
ALTER TABLE `survey_in_progress`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `teacher`
--
ALTER TABLE `teacher`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `teacher_classes`
--
ALTER TABLE `teacher_classes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `teacher_page_mark`
--
ALTER TABLE `teacher_page_mark`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT pour la table `user_page`
--
ALTER TABLE `user_page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT pour la table `user_response`
--
ALTER TABLE `user_response`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT pour la table `user_sheet`
--
ALTER TABLE `user_sheet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `user_student`
--
ALTER TABLE `user_student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `user_teacher`
--
ALTER TABLE `user_teacher`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `user_valid_survey`
--
ALTER TABLE `user_valid_survey`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `websitecounter`
--
ALTER TABLE `websitecounter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `anonymous_id`
--
ALTER TABLE `anonymous_id`
  ADD CONSTRAINT `FK_FA93803B3FE509D` FOREIGN KEY (`survey_id`) REFERENCES `sheet` (`id`);

--
-- Contraintes pour la table `anonymous_id_page`
--
ALTER TABLE `anonymous_id_page`
  ADD CONSTRAINT `FK_6A211685C4663E4` FOREIGN KEY (`page_id`) REFERENCES `page` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_6A211685DC92DCD1` FOREIGN KEY (`anonymous_id_id`) REFERENCES `anonymous_id` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `classes`
--
ALTER TABLE `classes`
  ADD CONSTRAINT `FK_2ED7EC5C32A47EE` FOREIGN KEY (`school_id`) REFERENCES `school` (`id`);

--
-- Contraintes pour la table `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `FK_9474526CC4663E4` FOREIGN KEY (`page_id`) REFERENCES `page` (`id`);

--
-- Contraintes pour la table `page`
--
ALTER TABLE `page`
  ADD CONSTRAINT `FK_140AB62023EDC87` FOREIGN KEY (`subject_id`) REFERENCES `subject` (`id`),
  ADD CONSTRAINT `FK_140AB62041807E1D` FOREIGN KEY (`teacher_id`) REFERENCES `teacher` (`id`),
  ADD CONSTRAINT `FK_140AB6208B1206A5` FOREIGN KEY (`sheet_id`) REFERENCES `sheet` (`id`);

--
-- Contraintes pour la table `page_question`
--
ALTER TABLE `page_question`
  ADD CONSTRAINT `FK_36684E6E1E27F6BF` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_36684E6EC4663E4` FOREIGN KEY (`page_id`) REFERENCES `page` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `question_standard`
--
ALTER TABLE `question_standard`
  ADD CONSTRAINT `FK_73FF678B1E27F6BF` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`);

--
-- Contraintes pour la table `response`
--
ALTER TABLE `response`
  ADD CONSTRAINT `FK_3E7B0BFB1E27F6BF` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`);

--
-- Contraintes pour la table `school_subject`
--
ALTER TABLE `school_subject`
  ADD CONSTRAINT `FK_69E10CE2C32A47EE` FOREIGN KEY (`school_id`) REFERENCES `school` (`id`);

--
-- Contraintes pour la table `sheet`
--
ALTER TABLE `sheet`
  ADD CONSTRAINT `FK_873C91E29E225B24` FOREIGN KEY (`classes_id`) REFERENCES `classes` (`id`);

--
-- Contraintes pour la table `student_classes`
--
ALTER TABLE `student_classes`
  ADD CONSTRAINT `FK_EFDBCB3D9E225B24` FOREIGN KEY (`classes_id`) REFERENCES `classes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_EFDBCB3DCB944F1A` FOREIGN KEY (`student_id`) REFERENCES `student` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `survey_in_progress`
--
ALTER TABLE `survey_in_progress`
  ADD CONSTRAINT `FK_DD00511FB3FE509D` FOREIGN KEY (`survey_id`) REFERENCES `sheet` (`id`);

--
-- Contraintes pour la table `teacher_classes`
--
ALTER TABLE `teacher_classes`
  ADD CONSTRAINT `FK_CF2FF38423EDC87` FOREIGN KEY (`subject_id`) REFERENCES `subject` (`id`),
  ADD CONSTRAINT `FK_CF2FF38441807E1D` FOREIGN KEY (`teacher_id`) REFERENCES `teacher` (`id`),
  ADD CONSTRAINT `FK_CF2FF3849E225B24` FOREIGN KEY (`classes_id`) REFERENCES `classes` (`id`),
  ADD CONSTRAINT `FK_CF2FF384B79F5C75` FOREIGN KEY (`school_subject_id`) REFERENCES `school_subject` (`id`);

--
-- Contraintes pour la table `teacher_page_mark`
--
ALTER TABLE `teacher_page_mark`
  ADD CONSTRAINT `FK_9BDBA2B041807E1D` FOREIGN KEY (`teacher_id`) REFERENCES `teacher` (`id`),
  ADD CONSTRAINT `FK_9BDBA2B0C4663E4` FOREIGN KEY (`page_id`) REFERENCES `page` (`id`);

--
-- Contraintes pour la table `teacher_subject`
--
ALTER TABLE `teacher_subject`
  ADD CONSTRAINT `FK_360CB33B23EDC87` FOREIGN KEY (`subject_id`) REFERENCES `subject` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_360CB33B41807E1D` FOREIGN KEY (`teacher_id`) REFERENCES `teacher` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `FK_8D93D649C32A47EE` FOREIGN KEY (`school_id`) REFERENCES `school` (`id`);

--
-- Contraintes pour la table `user_page`
--
ALTER TABLE `user_page`
  ADD CONSTRAINT `FK_6E8BFAE9C4663E4` FOREIGN KEY (`page_id`) REFERENCES `page` (`id`),
  ADD CONSTRAINT `FK_6E8BFAE9DC92DCD1` FOREIGN KEY (`anonymous_id_id`) REFERENCES `anonymous_id` (`id`);

--
-- Contraintes pour la table `user_response`
--
ALTER TABLE `user_response`
  ADD CONSTRAINT `FK_DEF6EFFBC4663E4` FOREIGN KEY (`page_id`) REFERENCES `page` (`id`),
  ADD CONSTRAINT `FK_DEF6EFFBDC92DCD1` FOREIGN KEY (`anonymous_id_id`) REFERENCES `anonymous_id` (`id`),
  ADD CONSTRAINT `FK_DEF6EFFBFBF32840` FOREIGN KEY (`response_id`) REFERENCES `response` (`id`);

--
-- Contraintes pour la table `user_sheet`
--
ALTER TABLE `user_sheet`
  ADD CONSTRAINT `FK_65FE6ABA8B1206A5` FOREIGN KEY (`sheet_id`) REFERENCES `sheet` (`id`),
  ADD CONSTRAINT `FK_65FE6ABADC92DCD1` FOREIGN KEY (`anonymous_id_id`) REFERENCES `anonymous_id` (`id`);

--
-- Contraintes pour la table `user_student`
--
ALTER TABLE `user_student`
  ADD CONSTRAINT `FK_EF2EB139A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `FK_EF2EB139CB944F1A` FOREIGN KEY (`student_id`) REFERENCES `student` (`id`);

--
-- Contraintes pour la table `user_teacher`
--
ALTER TABLE `user_teacher`
  ADD CONSTRAINT `FK_E8FBB8DF41807E1D` FOREIGN KEY (`teacher_id`) REFERENCES `teacher` (`id`),
  ADD CONSTRAINT `FK_E8FBB8DFA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `user_valid_survey`
--
ALTER TABLE `user_valid_survey`
  ADD CONSTRAINT `FK_78E09C08B1206A5` FOREIGN KEY (`sheet_id`) REFERENCES `sheet` (`id`),
  ADD CONSTRAINT `FK_78E09C0CB944F1A` FOREIGN KEY (`student_id`) REFERENCES `student` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
