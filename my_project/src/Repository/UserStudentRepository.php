<?php

namespace App\Repository;

use App\Entity\UserStudent;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method UserStudent|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserStudent|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserStudent[]    findAll()
 * @method UserStudent[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserStudentRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UserStudent::class);
    }

    // /**
    //  * @return UserStudent[] Returns an array of UserStudent objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserStudent
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
