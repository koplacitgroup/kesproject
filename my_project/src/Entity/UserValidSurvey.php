<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserValidSurveyRepository")
 */
class UserValidSurvey
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Student", inversedBy="userValidSurveys")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Student;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Sheet", inversedBy="userValidSurveys")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Sheet;

    /**
     * @ORM\Column(type="boolean")
     */
    private $Validate;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStudent(): ?Student
    {
        return $this->Student;
    }

    public function setStudent(?Student $Student): self
    {
        $this->Student = $Student;

        return $this;
    }

    public function getSheet(): ?Sheet
    {
        return $this->Sheet;
    }

    public function setSheet(?Sheet $Sheet): self
    {
        $this->Sheet = $Sheet;

        return $this;
    }

    public function getValidate(): ?bool
    {
        return $this->Validate;
    }

    public function setValidate(bool $Validate): self
    {
        $this->Validate = $Validate;

        return $this;
    }
}
