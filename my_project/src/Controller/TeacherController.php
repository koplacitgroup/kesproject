<?php

namespace App\Controller;

use App\Entity\Page;
use App\Entity\Sheet;
use App\Entity\Subject;
use App\Entity\Teacher;
use App\Service\EmailService;
use App\Service\TeacherService;
use App\Repository\PageRepository;
use App\Repository\SchoolRepository;
use App\Repository\CommentRepository;
use App\Repository\SubjectRepository;
use App\Repository\TeacherClassesRepository;
use App\Repository\TeacherPageMarkRepository;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\TeacherSurveyMarkRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class TeacherController extends AbstractController
{
    /**
    * @Route("/teacher/", name="teacher")
    */
    public function index(Request $request, CommentRepository $commentRepository,TeacherClassesRepository $teacherClassesRepository, TeacherPageMarkRepository $teacherPageMarkRepository, TeacherService $teacherService,PageRepository $pageRepository)
    {   
    
        //Main page of a teacher


        //Web page to select the survey that the teacher want to see more detail 
if(!$request->request->get('page_selected')){


        $teacher = ($this->getUser()->getUserTeacher())->getTeacher();

        $date = new \DateTime();
        $year = $date->format('Y');
      $surveyList = $teacherService->getSurveysForTeacher($teacher,$year);
      
      $data=$teacherService->getPageBySurveys($surveyList, $teacher);

      
      return $this->render('/teacher/pre.index.form.html.twig',['data'=> $data]);
       
}else{
  //Web page to display  infomation about a survey (marks , comments , statistics ...)
  $teacher = ($this->getUser()->getUserTeacher())->getTeacher();
  $page    = $pageRepository->find($request->request->get('page_selected'));
  $marklist =$teacherService->getMarkListofTeacherForOnePage($page);
  $mark  =$teacherPageMarkRepository->findOneByTeacherAndPage($teacher,$page);

        
  $teacherClassesList = $teacher->getTeacherClasses();
  $school = $this->getUser()->getSchool();

  
  $commentnumber = $commentRepository->countByPage($page);
  $randomcomment = $commentRepository->findOneByPage($page);
  dump($mark);
  dump($marklist);
if(!$mark){
    return $this->render('teacher/error_pages/error.not.found.mark.html.twig');
}
$graphdata = $teacherService->getMarksTeacherMainGraph($teacher,$page);
  return $this->render('teacher/index.html.twig', ['pageId'=>$page->getId(),
      'commentnumber' => $commentnumber, 'controller_name' => 'TeacherController', 'mark' => $mark, 'marklist' => $marklist, 'school' => $school, 'teacher' => $teacher, 'teacherClassesList' => $teacherClassesList, 'randomcomment'=>$randomcomment, 'graphdata'=>$graphdata]);
  
}      
  }
  
	/**
    * @Route("/teachercz/", name="teachercz")
    */
    public function indexcz(Request $request, CommentRepository $commentRepository,TeacherClassesRepository $teacherClassesRepository, TeacherPageMarkRepository $teacherPageMarkRepository, TeacherService $teacherService,PageRepository $pageRepository)
    {   
    
        //Main page of a teacher
if(!$request->request->get('page_selected')){


        $teacher = ($this->getUser()->getUserTeacher())->getTeacher();

        $date = new \DateTime();
        $year = $date->format('Y');
      $surveyList = $teacherService->getSurveysForTeacher($teacher,$year);
      
      $data=$teacherService->getPageBySurveys($surveyList, $teacher);

      
      return $this->render('/teacher/pre.index.formcz.html.twig',['data'=> $data]);
       
}else{
  //Get infomation about a survey
  $teacher = ($this->getUser()->getUserTeacher())->getTeacher();
  $page    = $pageRepository->find($request->request->get('page_selected'));
  $marklist =$teacherService->getMarkListofTeacherForOnePage($page);
  $mark  =$teacherPageMarkRepository->findOneByTeacherAndPage($teacher,$page);

        
  $teacherClassesList = $teacher->getTeacherClasses();
  $school = $this->getUser()->getSchool();

  
  $commentnumber = $commentRepository->countByPage($page);
 
  if(!$mark || !$marklist){
    return $this->render('teacher/error_pages/error.not.found.markcz.html.twig');
}

  return $this->render('teacher/indexcz.html.twig', ['pageId'=>$page->getId(),
      'commentnumber' => $commentnumber, 'controller_name' => 'TeacherController', 'mark' => $mark, 'marklist' => $marklist, 'school' => $school, 'teacher' => $teacher, 'teacherClassesList' => $teacherClassesList]);
  
}      
  }


    /**
     *@Route("teacher/stats/{id}", name="teacher_stats")
     */
    public function showStats(Page $page,CommentRepository $commentRepository, SubjectRepository $subjectRepository,Request $request)
    { //Show the stats of a teacher
        $teacher = ($this->getUser()->getUserTeacher())->getTeacher();
        

       

        if($request->request->get('class_selection'))
        {
            $classId=$request->request->get('class_selection');
            $classes= $classesRepository->find($classId);
            $comments =  $commentRepository->findByClasses($classes);
            return $this->render('teacher/stats.details.html.twig', ['pageId'=>$page->getId(),'teacher' => $teacher, 'classes' => $classes, 'subject' => null, 'comments'=>null]);
        }
        
        elseif($request->request->get('subject_selection')){
            $subjectId=$request->request->get('subject_selection');
            $subject = $subjectRepository->find($subjectId);
            return $this->render('teacher/stats.details.html.twig', ['pageId'=>$page->getId(),'teacher' => $teacher, 'classes' => $classes, 'subject' => null, 'comments'=>null]);

        }
         
            $comments = $commentRepository->findByPage($page);
            $commentnumber = $commentRepository->countByPage($page);

            return $this->render('teacher/stats.details.html.twig', ['pageId'=>$page->getId(),'commentnumber' => $commentnumber, 'teacher' => $teacher, 'classes' => null, 'subject' => null,'comments'=>$comments]);
       
        
        
    }

    /**
    *@Route("teacher/rating/{id}", name="school_rating")
    */
    public function SchoolSatisfyingRating(Page $page,TeacherService $teacherService,SchoolRepository $schoolRepository, TeacherPageMarkRepository $teacherPageMarkRepository)
    {    //See the Statistics of a school
        $school = $this->getUser()->getSchool();
        $schools = $schoolRepository->findAll();

        $marklist =$teacherService->getMarkListofTeacherForOnePage($page);
        $mark = $teacherPageMarkRepository->findOneByPage($page);

        return $this->render('teacher/school.satisfying.html.twig',
        ['school' => $school, 'schools' => $schools, 'mark' => $mark, 'marklist' => $marklist]);
    }

    /**
     * @Route("teacher/send/email/{id}", name="teacher_send_email")
     */
    public function senMail(Page $page,TeacherPageMarkRepository $teacherPageMarkRepository, EmailService $emailService, \Swift_Mailer $mailer){
        $teacher =$page->getTeacher();
        $teacherPageMark =$teacherPageMarkRepository->findOneByPage($page);
        $messageText = 'Teacher Information'."\n"
        .'-------------------------------------------------------------'."\n"
        . 'Teacher: '.$teacher->getFirstName().' '.$teacher->getLastName()."\n"
        .'Subject :'.$page->getSubject()->getName()."\n"
        .'Class : '.(($page->getSheet())->getClasses())->getName()."\n"
        .'School :'.((($page->getSheet())->getClasses())->getSchool())->getName()."\n"
        
        . '-------------------------------------------------------------'."\n"
        . 'Your mark: '.$teacherPageMark->getMark()."\n"
       
        . 'You have received feedback from: '.$teacherPageMark->getNumberOfResponses().' people'."\n"
        . '-------------------------------------------------------------'."\n";
    
    
        ;
        $email=$teacher->getEmail();
        $emailService->sendEmailtoTeacher($messageText,'kevinpizeuil@gmail.com',$mailer);
    return $this->render('test/hello.world.html.twig');
    }

    /**
     *@Route("chart" , name="chart")
     */
    public function chart(TeacherService $teacherService,PageRepository $pageRepository){
 $page= $pageRepository->find(51);
 $teacher=$page->getTeacher();
 $data = $teacherService->getMarksTeacherMainGraph($teacher,$page);
        return $this->render('test/chart.html.twig', ['data'=> $data]);
    }
}
