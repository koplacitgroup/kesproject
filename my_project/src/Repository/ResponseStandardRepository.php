<?php

namespace App\Repository;

use App\Entity\ResponseStandard;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ResponseStandard|null find($id, $lockMode = null, $lockVersion = null)
 * @method ResponseStandard|null findOneBy(array $criteria, array $orderBy = null)
 * @method ResponseStandard[]    findAll()
 * @method ResponseStandard[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ResponseStandardRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ResponseStandard::class);
    }

    // /**
    //  * @return ResponseStandard[] Returns an array of ResponseStandard objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ResponseStandard
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
