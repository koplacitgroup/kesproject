<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Entity\Teacher;
use App\Entity\UserTeacher;
use App\Entity\TeacherClasses;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/*This class if used to create fake data for the data base : 
  create some teachers for each school and linked them with some class.*/
class TeacherFixtures extends Fixture implements DependentFixtureInterface
{   
    public function __construct(UserPasswordEncoderInterface $encoder){
        $this->encoder = $encoder;
    }
    
    public function load(ObjectManager $manager)
    {
        $classExample = array( "10th", "11th" ,"12th");
        $faker = \Faker\Factory::create('fr_FR');
       for ($skl=1; $skl <=4 ; $skl++) { 


           foreach ($classExample as $key => $cl) {
            
            for ($clnb=1; $clnb <=4 ; $clnb++) { 
                for ($i=1; $i <=5 ; $i++) {
                    $gender = $faker->randomElement(array('male', 'female')); 
                    $teacher = new Teacher();
                    $teacher->setLastname($faker->lastname())   
                            ->setFirstname($faker->firstname($gender))
                            ->setEmail($faker->email());
                // $manager->persist($teacher);
                // $manager->flush();
        
                 $teacherClasses =new TeacherClasses();
                 $teacherClasses->setTeacher($teacher)
                                ->setClasses($this->getReference($cl.$skl.$clnb));
                if($i<4){
                    $teacherClasses->setSubject($this->getReference('subject'.mt_rand(9,12)));
                    $teacher->addSubject( $this->getReference('subject'.mt_rand(9,12)) );    
                }

             
                elseif($i>=4 && $i<8){
                    $teacherClasses->setSubject($this->getReference('subject'.mt_rand(1,4)));
                    $teacher->addSubject( $this->getReference('subject'.mt_rand(9,12)) );    
                }
                
                else{
                    $teacherClasses->setSubject($this->getReference('subject'.mt_rand(5,8)));
                    $teacher->addSubject( $this->getReference('subject'.mt_rand(9,12)) );    
                }
                
                $manager->persist($teacher);
                $manager->persist($teacherClasses);
                $manager->flush();
                $user = new User();
                   $userTeacher = new UserTeacher();

                $user->setLastname($teacher->getLastname())
                     ->setFirstname($teacher->getFirstname())
                     ->setGender($gender)
                     ->setEmail($teacher->getEmail())
                     ->setSchool(($this->getReference($cl.$skl.$clnb)->getSchool()))
                     ->setPassword($this->encoder->encodePassword($user, 'testtest'))
                     ->setRoles(array('ROLE_TEACHER'));
                     $userTeacher->setUser($user);
                     $userTeacher->setTeacher($teacher);
                $user->setUserTeacher($userTeacher);
                 
                  $manager->persist($user);
                  $manager->persist($userTeacher);
                  $manager->flush();
                }
            }
           }
      
        
    }

        
    }

    public function getDependencies()
    {
        return array(
            SubjectFixtures::class,
        );
    }
}
